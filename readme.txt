Implementation of an algorithm for computing the union, intersection
or difference of two polygons described in a 
Computing & Graphics (Vol 13, No 2, pp 167-183) 
by Avraham Margalit and Gary D. Knott.

http://www.sciencedirect.com/science/article/pii/0097849389900599

Setup

Get it from the repo to your local machine.

In order to test things a node.js project with Grunt is super useful.
But it is not critical. To check it out simply point the browser to
http://localhost/intersection/v5.html


Running Tests

Make sure that Apache server is started otherwise will end up with error of type:
"PhantomJS timed out, possibly due to a missing QUnit start() call"
because Grunt file actually runs tests via HTTP

To run the test on the command line:
>>> grunt test

JSHint

Simply issuing 
>>> grunt
runs the jshint on the whole project

Utility Installations

Install Grunt:
npm install -g grunt-cli

Here are ilnks to the old documentation:
https://msdn.microsoft.com/en-us/library/bb412535.aspx
https://msdn.microsoft.com/en-us/library/bb412413.aspx

