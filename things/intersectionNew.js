
function Point2D(x, y) {
    if ( arguments.length > 0 ) {
        this.x = x;
        this.y = y;
    }
}

Point2D.prototype.toString = function() {
    return this.x +", "+this.y;
};
Point2D.prototype.swap = function() {
    var x = this.x;
    var y = this.y;

    this.x = y;
    this.y = x
};
Point2D.prototype.equals = function( p2) {
    if (this.x == p2.x && this.y == p2.y){
		return true;
	} else {
		return false;
	}
};

////////////////////////////////////////////////////////////
function Vector2D(x, y) {
    if ( arguments.length > 0 ) {
        this.x = x;
        this.y = y;
    }
}
Vector2D.prototype.dot = function(vector) {
    return this.x*vector.x + this.y*vector.y;
};
Vector2D.prototype.cross = function(vector) {
    return this.x*vector.y - this.y*vector.x;
};
/////////////////////////////////////////////////////////////
function Polygon(points) {
    if ( arguments.length > 0 ) {
        this.init(points);
    }
}
Polygon.prototype.init = function(points) {

	if(points.length && points.length >2){
        // Init properties
        var points = svgNode.getAttributeNS(null, "points").split(/[\s,]+/);

        this.handles = new Array();
        for ( var i = 0; i < points.length; i += 2) {
            var x = parseFloat( points[i]   );
            var y = parseFloat( points[i+1] );
            
            this.handles.push( new Handle(x, y, this) );
        }
    } else {
        throw new Error("Invalid polygon initialization");
    }
};
/////////////////////////////////////////////////////////////
Polygon.prototype.isConcave = function() {
    var positive = 0;
    var negative = 0;
    var length = this.handles.length;

    for ( var i = 0; i < length; i++) {
        var p0 = this.handles[i].point;
        var p1 = this.handles[(i+1) % length].point;
        var p2 = this.handles[(i+2) % length].point;
        var v0 = Vector2D.fromPoints(p0, p1);
        var v1 = Vector2D.fromPoints(p1, p2);
        var cross = v0.cross(v1);
        
        if ( cross < 0 ) {
            negative++;
        } else {
            positive++;
        }
    }

    return ( negative != 0 && positive != 0 );
};


/*****
*
*   isConvex
*
*****/
Polygon.prototype.isConvex = function() {
    return !this.isConcave();
};
//////////////////////////////////////////////////////////////

function pointWithStatus(){
    this.type = "";
    this.point = "";
}
pointWithStatus.prototype.toString = function() {
    return this.point.x +", "+this.point.y;
};


function polygonResults(status) {
    this.status = status;
	this.polygons = new Array();
	this.polygons[0] = {};
    this.polygons[0].points = new Array();
}

function pointResults(status) {
    this.status = status;
    this.points = new Array();
}
pointResults.prototype.toString = function() {
    return this.points.toString();
};
pointResults.prototype.appendPoints = function(points) {
    this.points = this.points.concat(points);
};
pointResults.prototype.sortUpDown = function(p1, p2) {
    return p2.y - p1.y;
};
pointResults.prototype.sortDownUp = function(p1, p2) {
    return p1.y - p2.y;
};
pointResults.prototype.sortLeftRight = function(p1, p2) {
    return p2.x - p1.x;
};
pointResults.prototype.sortRightLeft = function(p1, p2) {
    return p1.x - p2.x;
};

var Intersection = new function (){
	var table4 = {};

	table4.union = "same";
	table4.intersection= "same";
	table4.amb= "opposite";
	table4.bma= "opposite";

	var table2 = {};

	table2.union = {};
	table2.union.a = "outside";
	table2.union.b = "outside";

	table2.intersection= {};
	table2.intersection.a= "inside";
	table2.intersection.b= "inside";

	table2.amb= {};
	table2.amb.a= "outside";
	table2.amb.b= "inside";

	table2.bma= {};
	table2.bma.a= "inside";
	table2.bma.b= "outside";

	/*****
	*
	*   init
	*
	*****/
	this.init = function(status) {
		/*var result = {};
		result.status = status;
		result.points = new Array();
		result.appendPoints = function(points) {
			this.points = this.points.concat(points);
		};
		result.toString = function() {
		    return this.points.toString();
		};
		return result;
		*/
		return new pointResults(status);		
	};

	/*
	 *   intersectLinePolygon
	 */
	this.intersectLinePolygon = function(a1, a2, points) {
	
	    var pointsCopy = points.slice();
	    
		var result = Intersection.init("No Intersection");
		var length = points.length;

        var reqDir = Intersection.getPolygonOrientationForLine(a1, a2);

        var orientation = Intersection.getOrientation(pointsCopy);

		if (orientation != reqDir){
			points = Intersection.changeOrientation(pointsCopy); 
		}       
        
		for ( var i = 0; i < length; i++ ) {
			var b1 = points[i];
			var b2 = points[(i+1) % length];
			var inter = Intersection.intersectLineLine(a1, a2, b1, b2);
			result.appendPoints(inter.points);
		}

		if ( result.points.length > 0 ) {
			result.status = "Intersection";
		}

		if ( result.points.length > 1 ) {

			if ( a1.y - a2.y  > 0 ) { // line goes down
				result.points.sort(result.sortUpDown);
				//return "clockwise";
				if ( result.points[0].y - result.points[1].y < 0 ){
					result.points = result.points.reverse();
				}
			} else if ( a1.y - a2.y  < 0 ) { // line goes up
				result.points.sort(result.sortDownUp);
				if ( result.points[0].y - result.points[1].y > 0 ){
					result.points = result.points.reverse();
				}
			} else { //line horizontal
				if ( a1.x - a2.x > 0 ){ //line goes right to left
					result.points.sort(result.sortRightLeft);
					if ( result.points[0].x - result.points[1].x < 0 ){
						result.points = result.points.reverse();
					}
				} else { //line goes left to right
					result.points.sort(result.sortLeftRight);
					if ( result.points[0].x - result.points[1].x > 0 ){
						result.points = result.points.reverse();
					}
				}
				
			}	   

			
		}

		return result;
	};

	/*
	 *   intersectLineLine
	 */
	this.intersectLineLine = function(a1, a2, b1, b2) {
	
	    a1.x = parseFloat(a1.x);
	    a1.y = parseFloat(a1.y);
	    
	    a2.x = parseFloat(a2.x);
	    a2.y = parseFloat(a2.y);
	    
	    b1.x = parseFloat(b1.x);
	    b1.y = parseFloat(b1.y);
	    
	    b2.x = parseFloat(b2.x);
	    b2.y = parseFloat(b2.y);
	    
	    
		var result;
		
		var ua_t = (b2.x - b1.x) * (a1.y - b1.y) - (b2.y - b1.y) * (a1.x - b1.x);
		var ub_t = (a2.x - a1.x) * (a1.y - b1.y) - (a2.y - a1.y) * (a1.x - b1.x);
		var u_b  = (b2.y - b1.y) * (a2.x - a1.x) - (b2.x - b1.x) * (a2.y - a1.y);

		if ( u_b != 0 ) {
			var ua = ua_t / u_b;
			var ub = ub_t / u_b;

			if ( 0 <= ua && ua <= 1 && 0 <= ub && ub <= 1 ) {
				result = Intersection.init("Intersection");
				result.points.push(
					new Point2D(
						a1.x + ua * (a2.x - a1.x),
						a1.y + ua * (a2.y - a1.y)
					)
				);
			} else {
				result = Intersection.init("No Intersection");
			}
		} else {
			if ( ua_t == 0 || ub_t == 0 ) {
				result = Intersection.init("Coincident");
			} else {
				result = Intersection.init("Parallel");
			}
		}

		return result;
	};

	/*
	 *   pointInPolygon
	 */
	this.pointInPolygon = function(points, point) {
		var length  = points.length;
		var counter = 0;
		var x_inter;

		var p1 = points[0];
		for ( var i = 1; i <= length; i++ ) {
			var p2 = points[i%length];

			if ( point.y > Math.min(p1.y, p2.y)) {
				if ( point.y <= Math.max(p1.y, p2.y)) {
					if ( point.x <= Math.max(p1.x, p2.x)) {
						if ( p1.y != p2.y ) {
							x_inter = (point.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y) + p1.x;
							if ( p1.x == p2.x || point.x <= x_inter) {
								counter++;
							}
						}
					}
				}
			}
			p1 = p2;
		}

		return ( counter % 2 == 1 );
	};
	/*
	this.pointInPolygon2 = function(points, point) 
	{
	  var i;
	  var j=points.length-1;
	  var inPoly=false;

	  for (i=0; i<points.length; i++) 
	  {
		if (points[i].x<point.x && points[j].x>=point.x 
		  || points[j].x<point.x && points[i].x>=point.x) 
		{
		  if (points[i].y+(point.x-points[i].y)/ 
			(points[j].x-points[i].x)*(points[j].y 
			  -points[i].y)<point.y) 
		  {
			inPoly=!inPoly; 
		  }
		}
		j=i; 
	  }
	  return inPoly; 
	}
	*/
	/*
	 *   intersectPolygonPolygon
	 */
	this.intersectPolygonPolygon = function(points1, points2) {
		var result = Intersection.init("No Intersection");
		var length = points1.length;

		for ( var i = 0; i < length; i++ ) {
			var a1 = points1[i];
			var a2 = points1[(i+1) % length]; // this one makes sure that polygon is closed(lastpointIndex+1%length =0
			var inter = Intersection.intersectLinePolygon(a1, a2, points2);
			
			result.appendPoints(inter.points);
			if( inter.status == "Intersection"){
				// check to see if the next points2 is within points1
				// if it is then it needs to be added to result
				if ( Intersection.pointInPolygon(points2, a2) ){
					result.appendPoints(a2);
				}
			}
		}

		if ( result.points.length > 0 )
			result.status = "Intersection";

		return result;

	};

	/*
	 *   intersectPolygonPolygon
	 */
	this.isOverlapped = function(points1, points2) {
		var result = Intersection.init("No Intersection");
		var length = points1.length;

		for ( var i = 0; i < length; i++ ) {
			var a1 = points1[i];
			var a2 = points1[(i+1) % length]; // this one makes sure that polygon is closed(lastpointIndex+1%length =0
			var inter = Intersection.intersectLinePolygon(a1, a2, points2);
			
			result.appendPoints(inter.points);
			if( inter.status == "Intersection"){
				return true;
			}
		}

		return false;

	};
    
    this.lineMidPoint = function(point1, point2){
    
        point1.point.x = parseFloat(point1.point.x);
	    point1.point.y = parseFloat(point1.point.y);
	    
	    point2.point.x = parseFloat(point2.point.x);
	    point2.point.y = parseFloat(point2.point.y);
	    
        var x = (point1.point.x +point2.point.x)/2;
        var y = (point1.point.y +point2.point.y)/2;
        return new Point2D(x,y);
    }
    /* 
	 *   changeOrientation
	 */
    this.changeOrientation = function (polygon){
        polygon = polygon.reverse();
        var fp = polygon.pop();
        polygon.unshift(fp);
        return polygon;
    }
	/* 
	 *   isPointOnBoundary
	 */
    this.isPointOnBoundary = function (point, points){
		var retVal = false;
        var length = points.length;
		for ( var i = 0; i < length; i++ ) {
			var a1 = points[i];
			var a2 = points[(i+1) % length];
			var crossproduct = (point.y - a1.y) * (a2.x - a1.x) - (point.x - a1.x) * (a2.y - a1.y);
			if(crossproduct == 0){
				return true;
			}

		}
		return retVal;

    }
	/* 
	 *   polyIncludeIntersections
	 *   needs both polygons to determine intersections
	 */
	this.polyIncludeIntersections = function(points1, points2, oper) {
		var poly = new Array();
		var j=0;
		var length = points1.length;

		for ( var i = 0; i < length; i++ ) {
			var a1 = points1[i];
			var a2 = points1[(i+1) % length]; // this one makes sure that polygon is closed(lastpointIndex+1%length =0
			
			poly[j] = new pointWithStatus();
			poly[j]["point"] = a1;
			if ( Intersection.pointInPolygon(points2, a1) ){
				if ( Intersection.isPointOnBoundary(a1, points2)){ // && oper=="union"
					poly[j]["type"] = "outside";
				} else {
					poly[j]["type"] = "inside";
				}
			} else {
				poly[j]["type"] = "outside";
			}
			j++;

			var inter = Intersection.intersectLinePolygon(a1, a2, points2);
			
			if( inter.status == "Intersection"){
			    for (var k=0; k<inter.points.length; k++){
				    poly[j] = new pointWithStatus();
				    poly[j]["point"] = inter.points[k];
				    poly[j]["type"] = "boundary";    
				    j++;
				}
			}
		}
		return poly;
	}
	/* 
	 *   extractFragments
	 *   need poly2 for boundary/boundary condition
	 */
	this.extractFragments = function(fragments, poly1, poly2, type) {

		//var type = table2[oper]["a"];
		length = poly1.length;
		for ( var i = 0; i < length; i++ ) {
			var a1 = poly1[i];
			var a2= poly1[(i+1) % length];
			if (a1.point.equals(a2.point)){
				continue;
			}
			if ( a1.type == type || a2.type == type ){
				// insertE(fragment)
				var fragment = {p0:[a1.point], p1:[a2.point]};
				fragments.push(fragment);

			} else if (a1.type == "boundary" && a2.type == "boundary"){
				// get mid point and test if it is in the point2 polygon
				// if it is insertE(fragment)
				
				var mp = Intersection.lineMidPoint(a1, a2);
				if ( Intersection.pointInPolygon(poly2, mp)){
					if ( type=="inside"){
						var fragment = {p0:[a1.point], p1:[a2.point]};
						fragments.push(fragment);
					}
				} else {
					if ( type=="outside"){
						var fragment = {p0:[a1.point], p1:[a2.point]};
						fragments.push(fragment);
					}
				}
			}
		}

		return fragments;
	}

	/* 
	 *   removeDuplicateFragments
	 *   //kill duplicate fragments, temp idea for boundary condition cases
	 */
	this.removeDuplicateFragments = function( fragments ) {

		length = fragments.length;
		var ind = 0
		while( ind < length ){
			
			var fragment = fragments[ind];
			var length = fragments.length;
			for(var i=0; i<length; i++){
				if( ind!=i && fragment.p0[0].equals(fragments[i].p0[0]) && fragment.p1[0].equals(fragments[i].p1[0]) ){
					//delete this fragment
					fragments.splice(i,1);
					length = fragments.length;
				}
			}
			ind++;			
		}
		return fragments;
	}
	/* 
	 *   processFragments
	 *   
	 */
	this.processFragments = function( fragments ) {

		//console.info ( fragments)
		
		fragments = Intersection.removeDuplicateFragments(fragments);

		var result = {};
		result.status = "";
		result.polygons = new Array();
		//result.polygons[0] = {};
		//result.polygons[0].points = new Array();

		result.status = "No Intersection"; // TODO: make it type of the operation (ie intersection or union)

		//var result = Intersection.init("No Intersection");

		length = fragments.length;

		//var i=0;
		var nfi =0;
		var di =0;
		var polyIndex =0;
		var newPolyogn = true;
		var foundReversed = false;
		while( fragments.length ){
			var a1 = fragments[nfi];

			fragments.splice(nfi,1);  // instead of this should just set it as "visited" so that can return multi polygons

			var nfi = Intersection.searchNextFragment(a1.p1, fragments);
			
			// search for points whose orientation could be reversed due to polygon self intersecting
			if (nfi == null){ // if still didn't find anything then it a new polygon
				nfi = Intersection.searchNextFragmentReversed(a1.p1, fragments);
				if (nfi != null){ // if still didn't find anything then it a new polygon
					var tSwap = fragments[nfi].p0;
					fragments[nfi].p0 = fragments[nfi].p1;
					fragments[nfi].p1 = tSwap;
					foundReversed = true;
				}
			}

			if ( newPolyogn == true  ){
				
				result.polygons[polyIndex] = {};
				result.polygons[polyIndex].points = new Array();

				result.polygons[polyIndex].points.push(new Point2D(a1.p0[0].x,a1.p0[0].y) );
				result.polygons[polyIndex].points.push(new Point2D(a1.p1[0].x,a1.p1[0].y) );
				newPolyogn = false;

			} else if( nfi == null ){
				nfi = 0;
				newPolyogn = true;
				polyIndex++;	
				
			}else {
				if( fragments.length){
					result.polygons[polyIndex].points.push(new Point2D(a1.p1[0].x,a1.p1[0].y) );
					
					if( !foundReversed ){
						foundReversed = false;
					}
				}
			}
			//i++;


		}

		return result;
	}
	/* 
	 *   intersectPolygons
	 */
	this.intersectPolygons = function(points1, points2, oper) {

		var leftPoly = [];
		var rightPoly = [];

		//var result = Intersection.init("No Intersection");
		//var length = points1.length;
		if (console ){
			console.info ( points1.toString() );
			console.info ( points2.toString() );
		}
		//////////////////////////////////////////////////////////
		var orientationA = Intersection.getOrientation(points1);
		var orientationB = Intersection.getOrientation(points2);

		if ( table4[oper] == "same" ){
			if (orientationA != orientationB){
				points2 = Intersection.changeOrientation(points2);
			}
		} else {
			if (orientationA == orientationB){
				points2 = Intersection.changeOrientation(points2);
			}
		}
			
		///////////////////////////////////////////////////////////
		var j=0;
		leftPoly = Intersection.polyIncludeIntersections(points1, points2, oper);

		//console.info( "rightPoly starts here ");
		/////////////////////////////////////////////////////////////////////////////////////////////////
		rightPoly = Intersection.polyIncludeIntersections(points2, points1, oper);
		////////////////////////////////////////////////////////////////////////////////////////////////////

		var fragments = Array();

		var type = table2[oper]["a"];
		fragments = Intersection.extractFragments(fragments, leftPoly, points2, type);

		//console.info ( {a:fragments});
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		type = table2[oper]["b"];
		fragments = Intersection.extractFragments(fragments, rightPoly, points1, type);
		//console.info ( {b:fragments});
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		var result = Intersection.processFragments(fragments);

		//if ( result.points.length > 0 )
			//result.status = oper;
        
		//this is just swapping long/lat to lat/long
		/*
        length = result.points.length;
        for ( var i=0; i< length; i++){
            result.points[i].swap();
        }*/


		return result;

	}

	/* 
	 *   searchNextFragment
	 */
	this.searchNextFragment = function(point, fragments){
		var length = fragments.length;

		for ( var i = 0; i < length; i++ ) {
			var a1 = fragments[i];

			if( a1.p0[0].x == point[0].x && a1.p0[0].y == point[0].y){
				return i;
			}
		}
		return null;
	}
	/* 
	 *   searchNextFragmentReversed
	 *   for self intersectecting polygons. where orientation of an original polygon 
	 *   could be broken into several orientations of polygons seaparated at intersection points
	 *   and these orientations will be different
	 */
	this.searchNextFragmentReversed = function(point, fragments){
		var length = fragments.length;

		for ( var i = 0; i < length; i++ ) {
			var a1 = fragments[i];

			if( a1.p1[0].x == point[0].x && a1.p1[0].y == point[0].y){
				return i;
			}
		}
		return null;
	}
    /* 
	 *   getPolygonOrientationForLine
	 */
	this.getPolygonOrientationForLine = function(point1, point2){
	    point1.x = parseFloat(point1.x);
	    point1.y = parseFloat(point1.y);
	    point2.x = parseFloat(point2.x);
	    point2.y = parseFloat(point2.y);
	    
	    if (point1.y - point2.y > 0 ) { // line goes down
	        return "clockwise";
	    } else if (point1.y - point2.y == 0 ) { // line horizontal
	        if(point1.x - point2.x > 0 ) { // line goes left
	            return "clockwise";
	        } else {
	            return "counterclockwise";
	        }
	    } else {
	        return "counterclockwise";
	    }	    
	}
	
	/* 
	 *   getOrientation
	 * A slightly faster method is based on the observation that it isn't necessary to compute the area. 
	 * Find the lowest vertex (or, if there is more than one vertex with the same lowest coordinate, 
	 * the rightmost of those vertices) and then take the cross product of the edges fore and aft of it. 
	 * Both methods are O(n) for n vertices, but it does seem a waste to add up the total area when a single cross product 
	 (of just the right edges) suffices. Code for this is available at ftp://cs.smith.edu/pub/code/polyorient.C (2K).

The reason that the lowest, rightmost (or any other such extreme) point works is that the internal angle at this vertex is necessarily convex, strictly less than pi (even if there are several equally-lowest points). 
	 */
	this.getOrientation = function(polygon){
		/*
		if (Intersection.getArea(polygon) > 0){
			return "counterclockwise";
		} else {
			return "clockwise";
		}
		*/
		var length = polygon.length;
		
		var lpi = Intersection.getLowestPointIndex(polygon);

        var pb = lpi -1;
        var pa = lpi +1;
        
		if ( lpi == 0 ){
		    pb = length - 1;
		} else if ( lpi == length- 1){
		    pa = 0;
		}
		//console.info("poly "+ polygon.length);
		//console.info (lpi);
		var orient = Intersection.get3PProduct( polygon[pb], polygon[lpi], polygon[pa] );
		
		if ( orient > 0 ){
			return "counterclockwise";
		} else {
			return "clockwise";
		}
	}
	/////////////////////////////////////////////////////////////////////////
	this.getOrientationArea = function (polygon){
		/*
		 * orientation
		 *
		 * Return either clockwise or counter_clockwise for the orientation
		 * of the polygon.
		 */
		
		var length = polygon.length;
		var area = 0;
		
		// Do the wrap-around first 
		area = polygon[length-1].x * polygon[0].y - polygon[0].x * polygon[length-1].y;

		for ( var i = 0; i < length-1; i++ ) {

			area += polygon[i].x * polygon[i+1].y - polygon[i+1].x * polygon[i].y;

		}

		if (area >= 0.0){
			return "counterclockwise";
		}else{
			return "clockwise";
		}

	}
	/////////////////////////////////////////////////////////////////////////
    
    // x is longitude
    // y is latitude
    this.getLowestPointIndex = function(polygon){
        var length = polygon.length;
        
        var resI;
        var xMin, yMin;
        
        for ( var i = 0; i < length; i++ ) {
            if ( i==0 ){
                xMin = polygon[i].x;
                yMin = polygon[i].y;
                resI = 0;
                continue;
            }
            if (polygon[i].y < yMin ){
                xMin = polygon[i].x;
                yMin = polygon[i].y;
                resI = i;
            } else if ( polygon[i].y == yMin ){
                if ( polygon[i].x > xMin ){
                    xMin = polygon[i].x;
                    yMin = polygon[i].y;
                    resI = i;
                }
            }
		}
			
        return resI;
    }
    
    this.get3PProduct = function( p1, p2, p3){
        var s1 = p2.x*p3.y + p1.x*p2.y + p1.y*p3.x;
        var s2 = p1.y*p2.x + p2.y*p3.x + p1.x*p3.y;
        
        return s1 -s2;
    }
    
	/* 
	 *   getArea
	 */
	this.getArea = function(polygon){
            
            console.info("this one");
		var area   = 0;
		var length = polygon.length;
		var neg    = 0;
		var pos    = 0;

		for ( var i = 0; i < length; i++ ) {
			var h1 = polygon[i];
			var h2 = polygon[(i+1) % length];

			area += (h1.x * h2.y - h2.x * h1.y);
		}

		return area / 2;
	}

} ();


function getMePoly( aString ){
	var result = [];
	//var pl1 = "-118.32734997128485, 34.05709798926945,-118.33665847778322, 34.04298753935195,-118.35042840243692, 34.02210514224573,-118.3397975394839, 34.02210514224573,-118.33889007568361, 34.022786817001986,-118.31691741943361, 34.039289175269076,-118.28962326049805, 34.022502265437836,-118.28897769798763, 34.02210514224573,-118.27343836082638, 34.02210514224573";
	var pla1 = aString.split(",");
	for( var i=0; i<pla1.length; i= i+2){
		result.push( new Point2D( parseFloat(pla1[i+1]), parseFloat(pla1[i]) ) );
	}
	return result;
}

function getMePolyFromMap( arr ){
	var result = [];
	for( var i=0; i<arr.length; i++){
		var pla1 = arr[i];
		result.push( new Point2D( parseFloat(pla1.Latitude), parseFloat(pla1.Longitude) ) );
	}
	return result;
}
var poly1 = [new Point2D(1,1),new Point2D(1,3),new Point2D(3,3),new Point2D(3,1)];
var poly4 = Intersection.changeOrientation(poly1);
var poly2 = [new Point2D(2,2),new Point2D(2,4),new Point2D(4,4),new Point2D(4,2)];

var poly3 = [new Point2D(3,4),new Point2D(4,4),new Point2D(4,2)];


//test case 1
var pl1 = "-118.33099365234375, 34.013965274912636,-118.33236694335938, 33.896637128110214,-118.33374023437503, 33.77914733128647,-118.13323974609376, 33.89891688437139,-117.93273925781251, 34.01851844336969,-117.93548583984376, 33.9034762140408,-117.938232421875, 33.78827853625995";
var pl1_1 = "-118.28979492187499, 34.2617565244598,-118.30352783203126, 34.11141455542992,-118.30352783203126, 33.874240220452016,-118.15521240234375, 33.90005673964575,-118.14211121677592, 33.90233640448069,-118.03710937500003, 33.90233640448069,-118.03710937500003, 33.89093747081251,-117.82287597656249, 33.89093747081251,-117.82287597656249, 33.90233640448069,-117.76245117187501, 33.90233640448069";
var pl1_2 ="-118.20465087890625, 33.891451233247835,-118.15521240234375, 33.90005673964575,-118.14211121677592, 33.90233640448069,-118.11126708984375, 33.90233640448069,-118.11126708984375, 33.82935690773931,-117.84759521484375, 33.82935690773931,-117.84759521484375, 33.89093747081251,-117.82287597656249, 33.89093747081251,-117.82287597656249, 33.90233640448069,-117.76245117187501, 33.90233640448069,-117.84759521484375, 33.96036777801898,-117.84759521484375, 34.21861586197446,-118.11126708984375, 34.21861586197446,-118.11126708984375, 34.140077838008544,-118.20465087890625, 34.203725150921514,-118.20465087890625, 34.31848685867645,-118.43536376953126, 34.31848685867645";
var pl1_3="-118.38592529296876, 33.95646052534844,-118.38523864746095, 33.90575578744503,-118.38455200195314, 33.85502087238566,-118.31279754638675, 33.83705690642999,-118.24104309082031, 33.819089163941285,-118.23438897182731, 33.84589679025758,-118.19503784179688, 33.84589679025758,-118.19503784179688, 33.91601311340168,-118.2169788448769, 33.91601311340168,-118.20877075195315, 33.94905609818091,-118.22639643078715, 33.91601311340168,-118.28003853592033, 33.91601311340168,-118.28704833984375, 33.9479168983564,-118.28961158923731, 33.91601311340168,-118.33442687988284, 33.91601311340168,-118.33442687988284, 33.88604638944974,-118.33545684814453, 33.88466740785103";
var pl1_4="33.967361817064834, -116.97864532470705,33.960385479896885, -116.97864532470705,33.960385479896885, -116.97027051025393,33.94472223101991, -116.97027051025393,33.94472223101991, -116.97864532470705,33.937601619431724, -116.97864532470705,33.937601619431724, -116.9857200341797,33.967361817064834, -116.9857200341797";
var polyT1 = getMePoly(pl1_4);

//var pl2 = "-118.27400207519533, 34.02847764793555,-118.35330963134766, 34.02847764793555,-118.35330963134766, 34.051521610164926,-118.27400207519533, 34.051521610164926";
var pl2 = "-118.22937011718751, 34.04014265821753,-118.02337646484376, 34.04014265821753,-118.02337646484376, 33.76544869849222,-118.22937011718751, 33.76544869849222";
var pl2_1 = "-118.43536376953126, 34.31848685867645,-118.20465087890625, 34.31848685867645,-118.20465087890625, 33.85216970140742,-118.43536376953126, 33.85216970140742";
var pl2_2 = "-117.76519775390626, 34.04583232505719,-118.1414794921875, 34.04583232505719,-118.1414794921875, 34.19590137180651,-117.76519775390626, 34.19590137180651";
var pl2_3 = "-118.31451416015625, 33.88010707260322,-118.34678649902347, 33.88010707260322,-118.34678649902347, 33.9416510267666,-118.31451416015625, 33.9416510267666";
var pl2_4 = "33.99375764397386, -117.10601806640625,33.99375764397386, -116.97864532470705,33.9083202346629, -116.97864532470705,33.9083202346629, -117.10601806640625";
var polyT2 = getMePoly(pl2_4);

var rst = Intersection.intersectPolygons(polyT1, polyT2, "amb");

for (var i=0; i<rst.polygons.length ;i++ )
{
	console.info(rst.polygons[i].points.toString());
}
//console.info(rst.polygons[0].points.toString());
//console.info(rst.polygons[1].points.toString());

//console.info(Intersection.isOverlapped(poly1, poly2));
//console.info(Intersection.isOverlapped(poly1, poly3));

//console.info(Intersection.intersectPolygons(poly1, poly2, "bma"));
//console.info(Intersection.intersectPolygons(poly1, poly2, "amb"));
//console.info(Intersection.intersectPolygons(poly1, poly2, "union"));
//console.info(Intersection.intersectPolygons(poly1, poly2, "intersection"));
//console.info(Intersection.intersectPolygonPolygon(poly1, poly2));

/*
TODO: need to specify if the polygon in the resultset  need to be excluded or included. 
As in this "union" case:
p1:var pl1 = "-118.32734997128485, 34.05709798926945,-118.33665847778322, 34.04298753935195,-118.35042840243692, 34.02210514224573,-118.3397975394839, 34.02210514224573,-118.33889007568361, 34.022786817001986,-118.31691741943361, 34.039289175269076,-118.28962326049805, 34.022502265437836,-118.28897769798763, 34.02210514224573,-118.27343836082638, 34.02210514224573";
p2:var pl2 = "-118.27400207519533, 34.02847764793555,-118.35330963134766, 34.02847764793555,-118.35330963134766, 34.031521610164926,-118.27400207519533, 34.031521610164926";
*/