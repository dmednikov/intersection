String.prototype.namespace = function(separator) {
	var ns = this.split(separator || '.'), p = window, i; // ns is ["Denis", "yagoda", "tasty"]
	for (i = 0; i < ns.length; i++) {
		p = p[ns[i]] = p[ns[i]] || {};
	}
};

"Sphere".namespace();
"Sphere.view".namespace();
"Sphere.data".namespace();
"Sphere.callbacks".namespace();
"Sphere.templates".namespace();
"Sphere.utils".namespace();


Sphere.application = function(){
	//private space

	//public space
	return {
		btn1: "qwe",
		init: function(){
			alert("initialized");
		},
		merge: function(){
			this.templates["odna"]
		}
	};
}();

Sphere.templates.merge = function (container, template, data,indexes) {
	if (container.length) {
		data = data||{};	
		data._MODIFIERS = Sphere.templates.commonModifier;
		container.html( Sphere.templates[template].content.process(data));
		if (Sphere.templates[template].behaviours) 
			$.each( Sphere.templates[template].behaviours, function(index,func) {  if (!indexes || index in indexes) func(container, data);});
	}
	return container;
};	


Sphere.utils = function(){
	return{
		isValidURL : function (url){
			var rx = new RegExp("http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w-\\+ ./?%:&=#\\[\\]]*)?"); 
			var matches = rx.exec(url);
			return (matches != null && url == matches[0]); 			
		},
		getListNames :function(){
			var retVal ={}
			switch(Sphere.view.currentTab){
				case "relatedFrom":
					retVal.cSelections = "lpRelatedFrom";
					retVal.oSuggestions = "rpRelatedFrom";
					retVal.rLinks = "relatedFromRemoved";
				break;
				case "relatedWebNews":
					retVal.cSelections = "lpRelatedWebNews";
					retVal.oSuggestions = "rpRelatedWebNews";
					retVal.rLinks = "relatedWebNewsRemoved";
				break;
				case "relatedBlogPosts":
					retVal.cSelections = "lpRelatedBlogPosts";
					retVal.oSuggestions = "rpRelatedBlogPosts";
					retVal.rLinks = "relatedBlogPostsRemoved";
				break;
				default:
					retVal.cSelections = "lpRelatedFrom";
					retVal.oSuggestions = "rpRelatedFrom";
					retVal.rLinks = "relatedFromRemoved";
				break;
			}

			return retVal;
		},
		setupLeftPane : function(p){
			//////////////////////////////////////////////
			var el = $("#popURL", p).bind("click", function(e){

				var elem = $(".popup", p).parent();

				var x = e.pageX - 150;
				var y = e.pageY ;
				$("input#url",p).removeClass("entry-txt-error")
				if (elem.length){
					elem.children().css("top",y);
					elem.children().css("left",x);

					elem.show();
				} else {
					var urlPopup = jQuery("<div />").appendTo("body");
					var p = Sphere.templates.merge($(urlPopup),"urlpopup", {data:{one:"pervui",two:"vtoroi"}});
					//var x = e.pageX - this.offsetLeft;
					//var y = e.pageY - this.offsetTop;
					$(p).children().css("top",y);
					$(p).children().css("left",x);
				}

				$("input#url",p).val("");
				$("input#url",p).focus();
				//urlPopup.attr("id","pCloser");
				return false;
				
			});
			/////////////////////////////////////////////
			$("ul#ulCurSel", p).sortable({
				connectWith: ['ul#ulRemovedItems'],
				revert: true, 
				containment: 'document', 
				placeholder: "sort_placeholder",
				remove:function(e, ui){},
				//receive:function(e, ui){console.info("receive cursel");console.info(ui.item.prev());console.info(ui.item);console.info(ui.item.next());console.info(ui.sender);console.info(e);console.info(ui);}
				receive:function(e, ui){

					var listNames = Sphere.utils.getListNames();

					// get current item
					var ci = ui.item;
					//get the prev
					var pi = ui.item.prev();
					// know the current list to insert into(Sphere.data.lpRelatedFrom)
					// find node for prev item
					var n = null;
					
					// get the sender (to remove item from)
					var sender = ui.sender;
					var sID = sender.attr("id");
				
					var cNode;
					if(sID == "ulOtherSuggestions"){
						cNode = Sphere.data.find( listNames.oSuggestions, ci.attr("id") );
						Sphere.data.extract(cNode);
					}else if(sID == "ulRemovedItems"){
						cNode = Sphere.data.find(listNames.rLinks, ci.attr("id") );
						Sphere.data.extract(cNode);
					}
					var pNode = Sphere.data.find(listNames.cSelections, pi.attr("id") );
					if( cNode ){
						if(pNode ){
							//Sphere.data.lpRelatedFrom.add(cNode.elem,pNode);
							Sphere.data.add(listNames.cSelections,cNode.elem,pNode);
						}else{
							//Sphere.data.lpRelatedFrom.addBefore(cNode.elem);
							Sphere.data.addBefore(listNames.cSelections,cNode.elem);
						}
					}
			
					
				},
				update:function(e,ui){
					var listNames = Sphere.utils.getListNames();
					// get current item
					var ci = ui.item;
					//get the prev
					var pi = ui.item.prev();
					// know the current list to insert into(Sphere.data.lpRelatedFrom)
					// find node for prev item
					var n = null;
					var cNode;
					
					//cNode = Sphere.data.lpRelatedFrom.find(function(elem){ if ( elem.id==ci.attr("id") ) return true;},n);	
					cNode = Sphere.data.find(listNames.cSelections, ci.attr("id") );
					if( cNode ){ // this event fires before receive as well
						//cNode.extract();
						Sphere.data.extract(cNode);
						if(pi && pi.length){
							//var pNode = Sphere.data.lpRelatedFrom.find(function(elem){if ( elem.id==pi.attr("id") ) return true;},n);
							var pNode = Sphere.data.find(listNames.cSelections, pi.attr("id") );
							//Sphere.data.lpRelatedFrom.add(cNode.elem,pNode);
							Sphere.data.add(listNames.cSelections,cNode.elem,pNode);
						}else{
							//Sphere.data.lpRelatedFrom.addBefore(cNode.elem);
							Sphere.data.addBefore(listNames.cSelections,cNode.elem);
						}
					}
				}

			});
			$("ul#ulCurSel li a", p).each( 
				function(i){
					//$(this).cluetip({tracking: true, cluetipClass: 'rounded', dropShadow: false, showTitle: false});
					$(this).cluetip({tracking: true});
				}
			);
			//////////////////////////////////////////////
		},
		setupRightPane : function(p){
			//////////////////////////////////////////////
			$("ul#ulOtherSuggestions").sortable({ 
				connectWith: ['ul#ulCurSel'],
				revert: true, 
				containment: 'document', 
				//remove: function(e,ui) { console.log("removed ", ui.item); },
				placeholder: "sort_placeholder",
				remove:function(e, ui){},
				receive:function(e, ui){}
			});	
			/////////////////
			$("ul#ulRemovedItems").sortable({ 
				connectWith: ['ul#ulCurSel'],
				revert: true, 
				containment: 'document', 
				items: $("ul#ulRemovedItems>li",p).filter(":not(.iStatic)"),
				//remove: function(e,ui) { console.log("removed ", ui.item); },
				placeholder: "sort_placeholder",
				remove:function(e, ui){},
				receive:function(e, ui){

					var listNames = Sphere.utils.getListNames();
					// get current item
					var ci = ui.item;
					//get the prev
					var pi = ui.item.prev();
					// know the current list to insert into(Sphere.data.lpRelatedFrom)
					// find node for prev item
					var n = null;
					
					// get the sender (to remove item from)
					var sender = ui.sender;
					var sID = sender.attr("id");

					var cNode;
					if(sID == "ulCurSel"){
						cNode = Sphere.data.find(listNames.cSelections, ci.attr("id") );
						Sphere.data.extract(cNode);
					}
					var pNode = Sphere.data.find(listNames.rLinks, pi.attr("id") );
					if(pNode && pNode.length){
						//Sphere.data.relatedFromRemoved.add(cNode.elem,pNode);
						Sphere.data.add(listNames.rLinks,cNode.elem,pNode);
					}else{
						Sphere.data.addBefore(listNames.rLinks,cNode.elem);
						//Sphere.data.relatedFromRemoved.addBefore(cNode.elem);
					}
					
					
				}
			});

			///////////////////
			$("ul#ulOtherSuggestions li a", p).each( 
				function(i){
					//$(this).cluetip({tracking: true, cluetipClass: 'rounded', dropShadow: false, showTitle: false});
					$(this).cluetip({tracking: true});
					//$('ol:first a:last').cluetip({tracking: true});
				}
			);
			//////////////////////////////////////////////
		}
	}
}();

Sphere.data = function() {
	/////////  private space /////////
	var index=0;

	function incrementIndex(){ index++; }
	function getIndex(){ return index; }


	/*function initList( listName ){
		this[listName] = new DLList();
	}

	function addItem(listName, elem, pNode){
		this[listName].add(elem, pNode);
	}*/
	
	/////////  public space
	return {
		init : function(){
			this.relatedFromRemoved = new DLList();
			this.relatedWebNewsRemoved = new DLList();
			this.relatedBlogPostsRemoved = new DLList();
		},
		resetList : function( listName ){
			this[listName] = new DLList();
		},
		getList : function (listName){
			return this[listName];
		},
		getListAsArray : function (listName){
			var n= null;
			var any1 = {items:[]};
			while((n=this[listName].find(function(x){return true;},n))) {
				any1.items.push(n.elem);
			}
			return any1;
		},
		find: function (listName, id){
			var n=null;
			var node = this[listName].find(function(elem){if ( elem.id==id ) return true;},n);
			return node;
		},
		add: function(listName, elem, pNode){
			this[listName].add(elem, pNode);
		},
		addBefore: function(listName, elem, pNode){
			this[listName].addBefore( elem);
		},
		extract: function(node){
			node.extract();
		},
		serializeList: function(listName, glueProps, glueObjects){
			var n= null;
			var any1 = {items:[]};

			var lGlueProps = ";";
			if(glueProps){
				lGlueProps = glueProps;
			}

			var lGlueObjects = "|";
			if(glueObjects){
				lGlueObjects = glueObjects;
			}

			var retVal = "";
			while((n=this[listName].find(function(x){return true;},n))) {
				//any1.items.push(n.elem);
				for (var name in n.elem){
					//console.info(name+':'+n.elem[name]);
					retVal += name+':'+n.elem[name];
					retVal += lGlueProps;
				}
				retVal = retVal.substr(0,retVal.length- lGlueProps.length)
				retVal += lGlueObjects;
			}
			retVal = retVal.substr(0,retVal.length-lGlueObjects.length)
			return retVal;
		},

		addNewItem: function(listName, elem, pNode){
			incrementIndex();
			this[listName].add(elem, pNode);
			return getIndex();
		},
		getNextIndex: function(){
			var ind = getIndex();
			return ++ind;
		}
	}

}();


$(document).ready(
	function(){

		$("a.tabItem", $(".tabList")).bind("click", function(){
			//nex.viewStatus.currentTab = $(this).attr("href").match(/#(\w*)Tab/)[1];
			//alert(this);   
			//alert( $(this).attr("href").match(/#(\w*)Tab/)[1] )
			$(".tabList li").each( function(i){
				//console.info(this); 
				$(this).removeClass("ui-tabs-selected");
			});
			switch( $(this).attr("href").match(/#(\w*)Tab/)[1] ){
				case "relatedFrom":
					Sphere.view.currentTab = "relatedFrom";
					if(Sphere.data.lpRelatedFrom){
						var n = null;

						var any = Sphere.data.getListAsArray("lpRelatedFrom");

						var any1 = Sphere.data.getListAsArray("rpRelatedFrom");
			
						var any2 = Sphere.data.getListAsArray("relatedFromRemoved");

						var rpData = {"os":any1,"rem":any2};

		
						Sphere.templates.merge($(".leftPane"),"lp_relatedfromtab", any);
						Sphere.templates.merge($(".rightPane"),"rp_relatedfromtab", rpData);
					} else {
						//
						$.getJSON("getJSONList.php",Sphere.callbacks.lpRelatedFrom);
						$.getJSON("rpRelatedFromList.php",Sphere.callbacks.rpRelatedFrom);
					}
					$(this).parent().parent().addClass("ui-tabs-selected");
					$(this).parent().parent().blur();
				break;
				case "relatedWebNews":
					Sphere.view.currentTab = "relatedWebNews";
					if(Sphere.data.lpRelatedWebNews){

						var n = null;

						var any = Sphere.data.getListAsArray("lpRelatedWebNews");

						var any1 = Sphere.data.getListAsArray("rpRelatedWebNews");
				
						var any2 = Sphere.data.getListAsArray("relatedWebNewsRemoved");

						var rpData = {"os":any1,"rem":any2};

						Sphere.templates.merge($(".leftPane"),"lp_relatedwebnewstab", any);
						Sphere.templates.merge($(".rightPane"),"rp_relatedwebnewstab", rpData);
					} else {
						//
						$.getJSON("lpRelatedWebNewsList.php",Sphere.callbacks.lpRelatedWebNews);
						$.getJSON("rpRelatedWebNewsList.php",Sphere.callbacks.rpRelatedWebNews);
					}

					$(this).parent().parent().addClass("ui-tabs-selected");
					$(this).parent().parent().blur();
				break;
				case "relatedBlogPosts":
					Sphere.view.currentTab = "relatedBlogPosts";
					if(Sphere.data.lpRelatedBlogPosts){

						var listNames = Sphere.utils.getListNames();
						var n = null;

						var any = Sphere.data.getListAsArray(listNames.cSelections);

						var any1 = Sphere.data.getListAsArray(listNames.oSuggestions);
					
						var any2 = Sphere.data.getListAsArray(listNames.rLinks);

						var rpData = {"os":any1,"rem":any2};

						Sphere.templates.merge($(".leftPane"),"lp_relatedblogpoststab", any);
						Sphere.templates.merge($(".rightPane"),"rp_relatedblogpoststab", rpData);
					} else {

						$.getJSON("lpRelatedBlogPostsList.php",Sphere.callbacks.lpRelatedBlogPosts);
						$.getJSON("rpRelatedBlogPostsList.php",Sphere.callbacks.rpRelatedBlogPosts);
					}
					$(this).parent().parent().addClass("ui-tabs-selected");
					$(this).parent().parent().blur();
				break;
				default:
					Sphere.templates.merge($(".leftPane"),"relatedfromtab");
					Sphere.templates.merge($(".rightPane"),"rp_relatedwebnewstab")
					$(this).parent().parent().addClass("ui-tabs-selected");
					$(this).parent().parent().blur();
				break;
			}
			return false;
		});

		$("#tRF", $(".tabList")).trigger("click");

		$("#btnUndoChanges", $(".footer")).bind("click", function(){
			$.getJSON("getJSONList.php",Sphere.callbacks.lpRelatedFrom);
			$.getJSON("rpRelatedFromList.php",Sphere.callbacks.rpRelatedFrom);

			$.getJSON("lpRelatedWebNewsList.php",Sphere.callbacks.lpRelatedWebNews);
			$.getJSON("rpRelatedWebNewsList.php",Sphere.callbacks.rpRelatedWebNews)

			$.getJSON("lpRelatedBlogPostsList.php",Sphere.callbacks.lpRelatedBlogPosts);
			$.getJSON("rpRelatedBlogPostsList.php",Sphere.callbacks.rpRelatedBlogPosts);
		});
		
		$("#btnCommitChanges", $(".footer")).bind("click", function(){
			var any = Sphere.data.serializeList("lpRelatedFrom",";","|\n");
			console.info(any)
			//any = any.items.join("|\n");
			//alert(any)
		});
	}
);


