//http://bytes.com/forum/thread90925.html


// Constructor for list nodes
// DLListNode(elem) - constructor. Creates unlinked node with elem as data
// Interface:
// extract() - unlinks node, linking its predecessor and successor.
// insertAfter(node) - insert node after this node, updating links.

function DLListNode(elem) {
	this.elem = elem;
	this.prev = this.next = null;
}

DLListNode.prototype.extract = function () {
	if (this.prev) {
		this.prev.next = this.next;
	}
	if (this.next) {
		this.next.prev = this.prev;
	}
	this.prev = this.next = null;
}

DLListNode.prototype.insertAfter = function (newNode) {
	if (this == newNode) { return; } // don't be daft!
	newNode.extract();
	newNode.prev = this;
	if (this.next) {
		newNode.next = this.next;
		this.next.prev = newNode;
	}
	this.next = newNode;
}
DLListNode.prototype.insertBefore = function (newNode) {
	if (this == newNode) { return; } // don't be daft!
	newNode.extract();
	newNode.next = this;
	if (this.prev) {
		newNode.prev = this.prev;
		this.prev.next = newNode;
	}
	this.prev = newNode;

}

// The list itself
// Interface:
// getFirst() - returns first node, or null if none
// getLast() - returns last node, or null if none
// add(elem[,afterNode]) - creates new node with elem as data and
// inserts it at end of list, or optionally
// after afterNode
// foreach(func) - calls func on all elements stored in list
// find(func[,afterNode]) - finds first node in list (optionally after
// afterNode) where func returns true when
// called on the element.
function DLList() {
	this.prev = this.next = this;
}

DLList.prototype.insertAfter = DLListNode.prototype.insertAfter;

DLList.prototype.insertBefore = DLListNode.prototype.insertBefore;

DLList.prototype.extract = DLListNode.prototype.extract;

DLList.prototype.getFirst = function () {
	return (this.next == this)?null:this.next;
};
DLList.prototype.getLast = function () {
	return (this.prev == this)?null:this.prev;
};
DLList.prototype.add = function (elem,afterNode) {
	var newNode = new DLListNode(elem);
	if (!afterNode) {
		if (this.prev) {
			afterNode = this.prev;
		} else {
			afterNode = this;
		}
	}
	afterNode.insertAfter(newNode);
	return newNode;
};

DLList.prototype.addBefore = function (elem, beforeNode) {
	var newNode = new DLListNode(elem);
	if (!beforeNode) {
		if (this.next) {
			beforeNode = this.next;
		} else {
			beforeNode = this;
		}
	}
	beforeNode.insertBefore(newNode);
	return newNode;
	/////////////
	var newNode = new DLListNode(elem);
	var firstNode = this.getFirst();
	firstNode.insertFirst(newNode,this);

	this.next = newNode;
	return newNode;
};

DLList.prototype.foreach = function (func) {
	for (var node = this.next;node != this;node = node.next) {
		func(node.elem);
	}
};

DLList.prototype.length = function () {
	var i=0;
	for (var node = this.next;node != this;node = node.next) {
		i++;
	}
	return i;
};

DLList.prototype.find = function (func, obj,  startNode) {
	if (!startNode) { startNode = this; }
		for (var node = startNode.next; node != this;node = node.next) {
			if (func(node.elem, obj )) {return node;}
	}
};