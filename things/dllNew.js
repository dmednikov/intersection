DLListNode = function( elem) {
	/////////  private space /////////
	this.elem = elem;
	this.prev = this.next = null;

	//function incrementIndex(){ index++; }
	//function getIndex(){ return index; }	
	/////////  public space
	return {
		extract : function(){
			if (this.prev) {
				this.prev.next = this.next;
			}
			if (this.next) {
				this.next.prev = this.prev;
			}
			this.prev = this.next = null;
		},

		insertAfter : function( newNode ){
			if (this == newNode) { return; } // don't be daft!
			newNode.extract();
			newNode.prev = this;
			if (this.next) {
				newNode.next = this.next;
				this.next.prev = newNode;
			}
			this.next = newNode;
		},

		insertBefore : function( newNode ){
			if (this == newNode) { return; } // don't be daft!
			newNode.extract();
			newNode.next = this;
			if (this.prev) {
				newNode.prev = this.prev;
				this.prev.next = newNode;
			}
			this.prev = newNode;
		}
	}

}();



// The list itself
// Interface:
// getFirst() - returns first node, or null if none
// getLast() - returns last node, or null if none
// add(elem[,afterNode]) - creates new node with elem as data and
// inserts it at end of list, or optionally
// after afterNode
// foreach(func) - calls func on all elements stored in list
// find(func[,afterNode]) - finds first node in list (optionally after
// afterNode) where func returns true when
// called on the element.

DLList = function( elem) {
	/////////  private space /////////
	this.prev = this.next = this;

	//function incrementIndex(){ index++; }
	//function getIndex(){ return index; }	
	/////////  public space
	return {
		extract : function(){
			if (this.prev) {
				this.prev.next = this.next;
			}
			if (this.next) {
				this.next.prev = this.prev;
			}
			this.prev = this.next = null;
		},

		insertAfter : function( newNode ){
			DLListNode.prototype.insertAfter( newNode );
		},
		
		insertBefore : function( newNode ){
			DLListNode.prototype.insertBefore( newNode );
		},

		extract : function(  ){
			DLListNode.prototype.extract(  );
		},

		getFirst : function( newNode ){
			return (this.next == this)?null:this.next;
		},

		getLast : function( newNode ){
			return (this.prev == this)?null:this.prev;
		},

		add : function(elem, afterNode ){
			var newNode = new DLListNode(elem);
			if (!afterNode) {
				if (this.prev) {
					afterNode = this.prev;
				} else {
					afterNode = this;
				}
			}
			afterNode.insertAfter(newNode);
			return newNode;
		},

		addBefore : function(elem, beforeNode ){
			var newNode = new DLListNode(elem);
			if (!beforeNode) {
				if (this.next) {
					beforeNode = this.next;
				} else {
					beforeNode = this;
				}
			}
			beforeNode.insertBefore(newNode);
			return newNode;
			/////////////
			var newNode = new DLListNode(elem);
			var firstNode = this.getFirst();
			firstNode.insertFirst(newNode,this);

			this.next = newNode;
			return newNode;
		},

		foreach : function( func ){
			for (var node = this.next;node != this;node = node.next) {
				func(node.elem);
			}
		},

		find : function( func, startNode ){
			if (!startNode) { startNode = this; }
				for (var node = startNode.next; node != this;node = node.next) {
					if (func(node.elem)) {return node;}
			}
		}
	}

}();