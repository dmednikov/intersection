
//http://www.kevlindev.com/


/*****
*
*   constructor
*
*****/
function Point2D(x, y) {
    if ( arguments.length > 0 ) {
        this.x = x;
        this.y = y;
    }
}


var Intersection = new function (){
	var table4 = {};

	table4.union = "same";
	table4.intersection= "same";
	table4.amb= "opposite";
	table4.bma= "opposite";

	var table2 = {};

	table2.union = {};
	table2.union.a = "outside";
	table2.union.b = "outside";

	table2.intersection= {};
	table2.intersection.a= "inside";
	table2.intersection.b= "inside";

	table2.amb= {};
	table2.amb.a= "outside";
	table2.amb.b= "inside";

	table2.bma= {};
	table2.bma.a= "inside";
	table2.bma.b= "outside";

	/*****
	*
	*   init
	*
	*****/
	this.init = function(status) {
		var result = {};
		result.status = status;
		result.points = new Array();
		result.appendPoints = function(points) {
			this.points = this.points.concat(points);
		};
		return result;
	};

	/*
	 *   intersectLinePolygon
	 */
	this.intersectLinePolygon = function(a1, a2, points) {
		var result = Intersection.init("No Intersection");
		var length = points.length;

		for ( var i = 0; i < length; i++ ) {
			var b1 = points[i];
			var b2 = points[(i+1) % length];
			var inter = Intersection.intersectLineLine(a1, a2, b1, b2);

			result.appendPoints(inter.points);
		}

		if ( result.points.length > 0 ) result.status = "Intersection";

		return result;
	};

	/*
	 *   intersectLineLine
	 */
	this.intersectLineLine = function(a1, a2, b1, b2) {
		var result;
		
		var ua_t = (b2.x - b1.x) * (a1.y - b1.y) - (b2.y - b1.y) * (a1.x - b1.x);
		var ub_t = (a2.x - a1.x) * (a1.y - b1.y) - (a2.y - a1.y) * (a1.x - b1.x);
		var u_b  = (b2.y - b1.y) * (a2.x - a1.x) - (b2.x - b1.x) * (a2.y - a1.y);

		if ( u_b != 0 ) {
			var ua = ua_t / u_b;
			var ub = ub_t / u_b;

			if ( 0 <= ua && ua <= 1 && 0 <= ub && ub <= 1 ) {
				result = Intersection.init("Intersection");
				result.points.push(
					new Point2D(
						a1.x + ua * (a2.x - a1.x),
						a1.y + ua * (a2.y - a1.y)
					)
				);
			} else {
				result = Intersection.init("No Intersection");
			}
		} else {
			if ( ua_t == 0 || ub_t == 0 ) {
				result = Intersection.init("Coincident");
			} else {
				result = Intersection.init("Parallel");
			}
		}

		return result;
	};

	/*
	 *   pointInPolygon
	 */
	this.pointInPolygon = function(points, point) {
		var length  = points.length;
		var counter = 0;
		var x_inter;

		var p1 = points[0];
		for ( var i = 1; i <= length; i++ ) {
			var p2 = points[i%length];

			if ( point.y > Math.min(p1.y, p2.y)) {
				if ( point.y <= Math.max(p1.y, p2.y)) {
					if ( point.x <= Math.max(p1.x, p2.x)) {
						if ( p1.y != p2.y ) {
							x_inter = (point.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y) + p1.x;
							if ( p1.x == p2.x || point.x <= x_inter) {
								counter++;
							}
						}
					}
				}
			}
			p1 = p2;
		}

		return ( counter % 2 == 1 );
	};

	/*
	 *   intersectPolygonPolygon
	 */
	this.intersectPolygonPolygon = function(points1, points2) {
		var result = Intersection.init("No Intersection");
		var length = points1.length;

		for ( var i = 0; i < length; i++ ) {
			var a1 = points1[i];
			var a2 = points1[(i+1) % length]; // this one makes sure that polygon is closed(lastpointIndex+1%length =0
			var inter = Intersection.intersectLinePolygon(a1, a2, points2);
			
			result.appendPoints(inter.points);
			if( inter.status == "Intersection"){
				// check to see if the next points2 is within points1
				// if it is then it needs to be added to result
				if ( Intersection.pointInPolygon(points2, a2) ){
					result.appendPoints(a2);
				}
			}
		}

		if ( result.points.length > 0 )
			result.status = "Intersection";

		return result;

	};

	/*
	 *   intersectPolygonPolygon
	 */
	this.isOverlapped = function(points1, points2) {
		var result = Intersection.init("No Intersection");
		var length = points1.length;

		for ( var i = 0; i < length; i++ ) {
			var a1 = points1[i];
			var a2 = points1[(i+1) % length]; // this one makes sure that polygon is closed(lastpointIndex+1%length =0
			var inter = Intersection.intersectLinePolygon(a1, a2, points2);
			
			result.appendPoints(inter.points);
			if( inter.status == "Intersection"){
				return true;
			}
		}

		return false;

	};

	/* 
	 *   intersectPolygons
	 */
	this.intersectPolygons = function(points1, points2, oper) {

		var leftPoly = [];
		var rightPoly = [];

		var result = Intersection.init("No Intersection");
		var length = points1.length;
		
		//////////////////////////////////////////////////////////
		var orientationA = Intersection.getOrientation(points1);
		var orientationB = Intersection.getOrientation(points2);
		if ( table4[oper] == "same" ){
			if (orientationA != orientationB){
				//points2 = changeorientation(points2);
				points2.reverse();
			}
		} else {
			if (orientationA == orientationB){
				//points2 = changeorientation(points2);
				points2.reverse();
			}
		}
		///////////////////////////////////////////////////////////
		var j=0;
		for ( var i = 0; i < length; i++ ) {
			var a1 = points1[i];
			var a2 = points1[(i+1) % length]; // this one makes sure that polygon is closed(lastpointIndex+1%length =0
			
			leftPoly[j] = {};
			leftPoly[j]["point"] = a1;
			if ( Intersection.pointInPolygon(points2, a1) ){
				leftPoly[j]["type"] = "inside";
			} else {
				leftPoly[j]["type"] = "outside";
			}
			j++;

			var inter = Intersection.intersectLinePolygon(a1, a2, points2);
			
			if( inter.status == "Intersection"){
				leftPoly[j] = {};
				leftPoly[j]["point"] = inter.points[0];
				leftPoly[j]["type"] = "boundary";
				j++;
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////

		length = points2.length;
		j=0;
		for ( var i = 0; i < length; i++ ) {
			var a1 = points2[i];
			var a2 = points2[(i+1) % length]; // this one makes sure that polygon is closed(lastpointIndex+1%length =0
			
			rightPoly[j] = {};
			rightPoly[j]["point"] = a1;
			if ( Intersection.pointInPolygon(points1, a1) ){
				rightPoly[j]["type"] = "inside";
			} else {
				rightPoly[j]["type"] = "outside";
			}
			j++;

			var inter = Intersection.intersectLinePolygon(a1, a2, points1);
			
			if( inter.status == "Intersection"){
				rightPoly[j] = {};
				rightPoly[j]["point"] = inter.points[0];
				rightPoly[j]["type"] = "boundary";
				j++;
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////

		var fragments = Array();

		var type = table2[oper]["a"];
		length = leftPoly.length;
		for ( var i = 0; i < length; i++ ) {
			var a1 = leftPoly[i];
			var a2= leftPoly[(i+1) % length];

			if ( a1.type == type || a2.type == type ){
				// insertE(fragment)
				var fragment = {p0:[a1.point], p1:[a2.point]};
				fragments.push(fragment);

			} else if (a1.type == "boundary" && a2.type == "boundary"){
				// get mid point and test if it is in the point2 polygon
				// if it is insertE(fragment)
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////
		var type = table2[oper]["b"];
		length = rightPoly.length;
		for ( var i = 0; i < length; i++ ) {
			var a1 = rightPoly[i];
			var a2= rightPoly[(i+1) % length];

			if ( a1.type == type || a2.type == type ){
				// insertE(fragment)
				var fragment = {p0:[a1.point], p1:[a2.point]};
				fragments.push(fragment);

			} else if (a1.type == "boundary" && a2.type == "boundary"){
				// get mid point and test if it is in the point2 polygon
				// if it is insertE(fragment)
			}
		}

		var res = [];
		length = fragments.length;

		var i=0;
		var nfi =0;
		var di =0;
		while( fragments.length ){
			var a1 = fragments[nfi];

			fragments.splice(nfi,1);
			//delete fragments[nfi];

			//di = nfi;
			var nfi = Intersection.searchNextFragment(a1.p1, fragments);

			if ( i==0 ){
				result.points.push(new Point2D(a1.p0[0].x,a1.p0[0].y) );
				//res.push(a1.p0);
				result.points.push(new Point2D(a1.p1[0].x,a1.p1[0].y) );
				//res.push(a1.p1);
			} else {
				if( fragments.length){
					//res.push(a1.p1);
					result.points.push(new Point2D(a1.p1[0].x,a1.p1[0].y) );
				}
			}
			i++;


		}
		

		if ( result.points.length > 0 )
			result.status = oper;

		return result;

	}

	/* 
	 *   searchNextFragment
	 */
	this.searchNextFragment = function(point, fragments){
		var length = fragments.length;

		for ( var i = 0; i < length; i++ ) {
			var a1 = fragments[i];

			if( a1.p0[0].x == point[0].x && a1.p0[0].y == point[0].y){
				return i;
			}
		}
		return null;
	}

	/* 
	 *   getOrientation
	 */
	this.getOrientation = function(polygon){
		if (Intersection.getArea(polygon) < 0){
			return "clockwise";
		} else {
			return "counterclockwise";
		}
	}

	/* 
	 *   getArea
	 */
	this.getArea = function(polygon){
		var area   = 0;
		var length = polygon.length;
		var neg    = 0;
		var pos    = 0;

		for ( var i = 0; i < length; i++ ) {
			var h1 = polygon[i];
			var h2 = polygon[(i+1) % length];

			area += (h1.x * h2.y - h2.x * h1.y);
		}

		return area / 2;
	}

} ();

var poly1 = [new Point2D(1,1),new Point2D(1,3),new Point2D(3,3),new Point2D(3,1)];
var poly2 = [new Point2D(2,2),new Point2D(2,4),new Point2D(4,4),new Point2D(4,2)];

var poly3 = [new Point2D(3,4),new Point2D(4,4),new Point2D(4,2)];

var poly4 = [new Point2D(33.954752,-118.082428),new Point2D(33.954752,-118.185425), new Point2D(34.000307,-118.185425), new Point2D(34.000307,-118.141478),new Point2D(34.014536,-118.141478),new Point2D(34.014536,-118.082428)];


var poly5 = [new Point2D(34.01681103381635,-118.17031860351564),new Point2D(34.01681103381635,-118.06732177734375), new Point2D(33.97098272249384,-118.06732177734375), new Point2D(33.97098272249384,-118.17031860351564)];


console.info(Intersection.isOverlapped(poly1, poly2));

console.info(Intersection.isOverlapped(poly4, poly5));

console.info(Intersection.intersectPolygons(poly4, poly5, "intersection"));

console.info(Intersection.intersectPolygonPolygon(poly1, poly2));

