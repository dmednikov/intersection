/*
@ikostenko: I believe the strange behavior you�re seeing is due to your new Selenium 
command not following the semantics for the different �types� of commands, i.e. �actions� and �accessors�. 
Actions are named starting with �do�, while accessors are named starting with �get�. 
You can see in selenium-commandhandlers.js that the two are registered differently 
(look for _registerAllAccessors compared to _registerAllActions).
*/





Selenium.prototype.doRightClick = function(locator) {
	var element = this.page().findElement(locator);
	//this.page()._fireEventOnElement("contextmenu", element, 0, 0); 

	var evt = element.ownerDocument.createEvent('MouseEvents');

	var RIGHT_CLICK_BUTTON_CODE = 2; // the same for FF and IE

	evt.initMouseEvent('click', true, true,
	element.ownerDocument.defaultView, 1, 0, 0, 0, 0, false,
	false, false, false, RIGHT_CLICK_BUTTON_CODE, null);

	if (document.createEventObject){
		// dispatch for IE
		element.fireEvent('onclick', evt)
	}
	else{
		// dispatch for firefox + others
		!element.dispatchEvent(evt);
	}
};
Selenium.prototype.doRightClickAt = function(locator, paramString) {
	
	if (!paramString){
		paramString = '200, 200';
	}

	var element = this.page().findElement(locator);
	var clientXY = getClientXY(element, paramString);

	var evt = element.ownerDocument.createEvent('MouseEvents');

	var RIGHT_CLICK_BUTTON_CODE = 2; // the same for FF and IE

	//this.page().map instead of element.ownerDocument.defaultView or window doesn't do anything
	//window.echo ("asd");

	evt.initMouseEvent('click', true, true, element.ownerDocument.defaultView, 1, 0, 0, clientXY[0], clientXY[1], false,false, false, false, RIGHT_CLICK_BUTTON_CODE, null);

	if (document.createEventObject){
		// dispatch for IE
		element.fireEvent('onclick', evt);
	}
	else{
		// dispatch for firefox + others
		var canceled = !element.dispatchEvent(evt);
		/*if(canceled) {
			// A handler called preventDefault
			alert("canceled");
		} else {
			// None of the handlers called preventDefault
			alert("not canceled");
		}*/

	}
};



PageBot.prototype.locateElementByJQuery = function(locator, inDocument) {
    var loc = locator.replace(/&gt;/g, '>');
    loc = loc.replace(/&lt;/g, '<');
    var element;
    try {
        element = $(inDocument).find(loc);
    } catch (e) {
        return null;
    }
    if (element.length == 1 ) {
        return element[0];
    } else if(element.length > 1) {
        return element.get();
    } else {
        return null;
    }
}
// The "inDocument" is a the document you are searching.
PageBot.prototype.locateElementByValueRepeated = function(text, inDocument) {
    // Create the text to search for
    var expectedValue = text + text;

    // Loop through all elements, looking for ones that have
    // a value === our expected value
    var allElements = inDocument.getElementsByTagName("*");
    for (var i = 0; i < allElements.length; i++) {
        var testElement = allElements[i];
        if (testElement.value && testElement.value === expectedValue) {
            return testElement;
        }
    }
    return null;
};


var str = "var loc = locator.replace(/&gt;/g, '>');loc = loc.replace(/&lt;/g, '<');var element;    try {        element = $(inDocument).find(loc);    } catch (e) {        return null;    }    if (element.length == 1 ) {        return element[0];    } else if(element.length > 1) {        return element.get();    } else {        return null;    }";

//Selenium.doAddLocationStrategy ("jquery", str);


Selenium.prototype.locateElementByJQuerySelector = function(locator, inDocument, inWindow) {

	

	this.doAddLocationStrategy ("jquery", str);

    var loc = locator.replace(/&gt;/g, '>');
    loc = loc.replace(/&lt;/g, '<');
    var element;
    try {
        element = $(inDocument).find(loc);
    } catch (e) {
        return null;
    }
    if (element.length == 1 ) {
        return element[0];
    } else if(element.length > 1) {
        return element.get();
    } else {
        return null;
    }
}
