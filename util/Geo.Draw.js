/*
 * Class used for drawing on the VEMap
 * 
 * TODO: Perhaps "map" should be aparameter into a constructor ?
 * @type type
 */
/* global Geo, map, VEShapeType */

Geo.Drawing = new function () { // jshint ignore:line

    var localDrawnShapesLayer = new VEShapeLayer();//var Geo.layers.DrawnShapes = new VEShapeLayer();
	localDrawnShapesLayer.SetTitle("selectable_shapes");
    var localBufferTempPoints = [];//var Geo.buffer.TempPoints = [];
    var localBufferTempShapes;//var Geo.buffer.TempShape = {};
    /*
     * Public function Draw. Attaches all the required events to the map
     * @returns {None}
     */
    this.Draw = function ()
    {
        try {
            map.AddShapeLayer(localDrawnShapesLayer);
        } catch (e) {
            //if shape layer already exists exception is thrown. 
            //no new layer is added
            //console.log(e);
        } finally {
            Geo.Selection.DetachSelectors(); // should really be "evented"
        }
        map.AttachEvent("onclick", EHDrawMouseClick);

    };
    /*
     * Public utility function to clean all local buffers
     */
    this.ClearAll = function () {
        localDrawnShapesLayer.DeleteAllShapes();
        localBufferTempPoints = [];
        localBufferTempShapes = {};
    };
    /*
     * Function in response to Cliick event on the map
     * 
     * @param {type} e
     * @returns {undefined}
     */
    EHDrawMouseClick = function (e)
    {
        var x = e.mapX;
        var y = e.mapY;
        var pixel = new VEPixel(x, y);

        var LL = map.PixelToLatLong(pixel);

        map.AttachEvent("onmousemove", EHDrawMouseMove);
        localBufferTempPoints.push(LL);

        if (e.rightMouseButton)
        {
            FinishDrawing();
        } else
        {
            //document.getElementById("divMap").style.cursor='crosshair';
        }
    };
    /*
     * Function in response to moving mouse (initialized in onmouselick event)
     * 
     * @param {type} e
     * @returns {undefined}
     */
    EHDrawMouseMove = function (e)
    {
        var x = e.mapX;
        var y = e.mapY;
        var pixel = new VEPixel(x, y);
        var LL = map.PixelToLatLong(pixel);

        var tempPoints = localBufferTempPoints.slice(0, localBufferTempPoints.length);

        tempPoints.push(LL);

        try
        {
            localDrawnShapesLayer.DeleteShape(localBufferTempShapes);
        } catch (ex)
        {
        }

        if (tempPoints.length === 2)
        {
            localBufferTempShapes = new VEShape(VEShapeType.Polyline, tempPoints);
            localBufferTempShapes.HideIcon();
            localDrawnShapesLayer.AddShape(localBufferTempShapes);
        }

        if (tempPoints.length > 2)
        {
            localBufferTempShapes = new VEShape(VEShapeType.Polygon, tempPoints);
            localBufferTempShapes.HideIcon();
            localDrawnShapesLayer.AddShape(localBufferTempShapes);
        }
    };
    /*
     * Function used to detach mouse events on the map
     * and draw-and-clear TempShape buffer
     */
    FinishDrawing = function () {

        DrawTempPointsShape();

        try
        {
            map.DetachEvent("onmousemove", EHDrawMouseMove);
            map.DetachEvent("onclick", EHDrawMouseClick);
            CleanUpAfterDrawingShape();
        } catch (err)
        {

        }
    };

    /*
     * Function to draw temp shape from buffer
     * and add it to the list of drawn shapes
     */
    DrawTempPointsShape = function ( ) {

        var tempo = new VEShape(VEShapeType.Polygon, localBufferTempPoints);
        tempo.SetDescription("<span id='polygonInfoBubble'></span><script>map.FireEvent('ShowPolygonInfoEvent');</script>");
        tempo.SetLineWidth(1);
        tempo.SetLineColor(new VEColor(0, 150, 100, 1.0));
        tempo.SetFillColor(new VEColor(0, 100, 150, 0.5));

        try
        {
            localDrawnShapesLayer.AddShape(tempo);
        } catch (ex)
        {
            console.info(ex);
        }
    };
    /*
     * Utility functino to clean up data structures after a temp shape has been drawn
     */
    CleanUpAfterDrawingShape = function () {
        try
        {
            localBufferTempPoints = [];
            localDrawnShapesLayer.DeleteShape(localBufferTempShapes);
            localBufferTempShapes = {};

        } catch (ex)
        {
        }
    };

    /*
     * Draws polygon ...
     */
    /*
     this.DrawPolygon = function () {
     
     try {
     map.AddShapeLayer(Geo.layers.DrawnShapes);
     } catch (e) {
     console.info(e);
     }
     
     DrawTempPointsShape();
     
     try {
     map.SetMapView(Geo.buffer.TempPoints);
     } catch (ex)
     {
     console.info(ex);
     }
     
     CleanUpAfterDrawingShape();
     };
     */
}();