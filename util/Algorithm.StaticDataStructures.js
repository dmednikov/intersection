/* 
 * Holds supporting data structure for the algorithm to work
 */

/* global Geo */
Algorithm = {};
Algorithm.StaticDataStructures = {};

/*
 * Table 1 from the paper.
 * Output polygon type for a given operation and given input polygon types
 */
Algorithm.StaticDataStructures.OutputPolygon = {};
Algorithm.StaticDataStructures.OutputPolygon.union = "same";
Algorithm.StaticDataStructures.OutputPolygon.intersection = "same";
Algorithm.StaticDataStructures.OutputPolygon.amb = "opposite";
Algorithm.StaticDataStructures.OutputPolygon.bma = "opposite";

/*
 * Table 2 from the paper.
 * Type of edge fragments to select according to the operation and 
 * the input polygon types
 */
Algorithm.StaticDataStructures.EdgeFragments = {};

Algorithm.StaticDataStructures.EdgeFragments.union = {};
Algorithm.StaticDataStructures.EdgeFragments.union.a = "outside";
Algorithm.StaticDataStructures.EdgeFragments.union.b = "outside";

Algorithm.StaticDataStructures.EdgeFragments.intersection = {};
Algorithm.StaticDataStructures.EdgeFragments.intersection.a = "inside";
Algorithm.StaticDataStructures.EdgeFragments.intersection.b = "inside";

Algorithm.StaticDataStructures.EdgeFragments.amb = {};
Algorithm.StaticDataStructures.EdgeFragments.amb.a = "outside";
Algorithm.StaticDataStructures.EdgeFragments.amb.b = "inside";

Algorithm.StaticDataStructures.EdgeFragments.bma = {};
Algorithm.StaticDataStructures.EdgeFragments.bma.a = "inside";
Algorithm.StaticDataStructures.EdgeFragments.bma.b = "outside";