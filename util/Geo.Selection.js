/* global Geo, map, Polygon, VEShapeType, geoModel */

Geo.Selection = new function () { // jshint ignore:line
    //var localPolygonsForAction = Geo.polygonsForAction;
    this.SelectShapes = function ()
    {
        map.AttachEvent("onclick", Geo.Selection.SelectPolygonsForAction);
		
		$( document ).on( "actionPolygonsUpdated", {
            foo: "bar"
        }, function( event ) {
            Geo.Selection.highlightActionPolygons.call(Geo.Selection);

        });
		
    };
    
    this.ClearAll = function () {
        Geo.Selection.DetachSelectors();
        Geo.layers.SelectionMarkers.DeleteAllShapes();
        //Geo.Selection.CleanUpHTML();
        //Geo.polygonsForAction = [];
    };
/*
    this.GetArea = function (id) {
        var shape = map.GetShapeByID(id);
        var points = shape.GetPoints();
        var t1 = getMePolyFromMap(points);//TOdO: refactor! This is a global function in Intersection.js, it should notbe like this
        document.getElementById("polArea").innerHTML = Polygon.getArea(t1);
    };
*/
/*
    this.GetConArea = function (id) {
        var shape = map.GetShapeByID(id);
        var points = shape.GetPoints();
        var t1 = getMePolyFromMap(points);
        document.getElementById("polArea").innerHTML = Polygon.getArea(t1);
    };
*/
    this.SelectPolygonsForAction = function (e) {
        if (e.rightMouseButton && e.elementID) {
			//console.info(e);
			var shape = map.GetShapeByID(e.elementID);
			//console.info(shape);
			var layerL = shape.GetShapeLayer();
			//debugger;
			//console.info(layerL);
			if( "selectable_shapes" === layerL.GetTitle()) {
				Geo.Selection.ClickedPolygonToActionArray(e.elementID);
			}
			//console.info(layerL.GetName());
            //Geo.Selection.AddPolygonToActionArray(e.elementID);
            
            
			//Geo.Selection.HighlightSelected(e.elementID); //has been refactored to an event
            //Geo.Selection.GetArea(e.elementID);
            //Geo.Selection.DisplayPolygonPoints();
            //Geo.Selection.DisplayCurrentPolygonPoints();
        }
    };
/*
    this.DisplayPolygonPoints = function () {
       var len = Math.min(2, Geo.polygonsForAction.length);
       for (var index = 0; index < len; index++) {
          var polygon = Geo.polygonsForAction[index].polygon;
          if (!polygon)
             break;
          var t1Len = polygon.length;
          var disp = "";
          var ltUSel = Geo.Utils.GetSelectedLatLongFormat();
          for (var i = 0; i < t1Len; i++) {
             if (ltUSel === "lnglat") {
                disp = disp + polygon[i].Longitude + ", " + polygon[i].Latitude + "<br />";
             } else {
                disp = disp + polygon[i] + "<br />";
             }

          }
          var m = index + 10;
          document.getElementById("pol" + m).innerHTML = disp;

       }
    };
*/
/*  
    this.DisplayCurrentPolygonPoints = function () {

        var polygon = Geo.polygonsForAction[Geo.polygonsForAction.length - 1].polygon;

        var t1Len = polygon.length;
        var disp = "";
        var ltUSel = Geo.Utils.GetSelectedLatLongFormat();
        for (var i = 0; i < t1Len; i++) {
            if (ltUSel === "lnglat") {
                disp = disp + polygon[i].Longitude + ", " + polygon[i].Latitude + "<br />";
            } else {
                disp = disp + polygon[i] + "<br />";
            }

        }
        document.getElementById("pol0").innerHTML = disp;
    };
*/
	this.reactToUpdateOfActionPolygons = function () {
		//console.info("registering this sucker");
        
    };
    /*
     * TODO: this function does more than just highlight !!! Need to refactor
     * @param {type} id
     * @returns {undefined} *
     */
	 /*
    this.AddPolygonToActionArray = function (id) {
       var shape = map.GetShapeByID(id);
       var points = shape.GetPoints();
       var t1 = points.slice(0, points.length);
       
       geoModel.addPolygonForAction({'id':id,'polygon':t1});
       
       return true;
    };*/
    /*
     * This function right polygon click in relation to ActionArray
     * that is either adds or removes polygom from the array
     * @param {type} id
     * @returns {undefined} *
     */
    this.ClickedPolygonToActionArray = function (id) {
       var shape = map.GetShapeByID(id);
       var points = shape.GetPoints();
       var t1 = points.slice(0, points.length);
       
       geoModel.handlePolygonForActionArray({'id':id,'polygon':t1});
       
       return true;
    };
	
	/*
     * highlights polygons in ActionPolygons array of the geoModel
     * @returns {undefined} *
     */
    this.highlightActionPolygons = function() {
		
        //Add drag handles
        try {
            // VEMap will throw exception on attempt to add existing layer, but will not add it
			//map.DeleteShapeLayer(Geo.layers.SelectionMarkers);
			Geo.layers.SelectionMarkers.DeleteAllShapes();
            map.AddShapeLayer(Geo.layers.SelectionMarkers);
        }
        catch (e) {
        }
		var polygons = geoModel._polygonsForAction;
		for(var pI = 0; pI < polygons.length; pI++){
			var points = polygons[pI].polygon;
			
			// the last point is already removed from the polygon
			for (i = 0; i <= (points.length - 1); i++) {
				var dragHandle = new VEShape(VEShapeType.Pushpin, points[i]);
				dragHandle.SetCustomIcon('images/y_green.png');
				var pixel = map.LatLongToPixel(points[i], map.GetZoomLevel());

				var title = "N: " + i + "<br />";
				title += points[i].toString();
				title += "<br />";
				title += pixel.x + ", " + pixel.y;
				dragHandle.SetTitle(title);
				Geo.layers.SelectionMarkers.AddShape(dragHandle);
			}
		
		}
        
    };
	
    /*
     * @param {type} id
     * @returns {undefined} *
     */
	 /*
    this.HighlightSelected = function (id) {
		
		return;
		
        var shape = map.GetShapeByID(id);
        var points = shape.GetPoints();

        //Add drag handles
        try {
            // VEMap will throw exception on attempt to add existing layer, but will not add it
            map.AddShapeLayer(Geo.layers.SelectionMarkers);
        }
        catch (e) {
        }
        // do not put the last marker onto the map, cause it will cover the 0'th
        for (i = 0; i <= (points.length - 2); i++) {
            var dragHandle = new VEShape(VEShapeType.Pushpin, points[i]);
            dragHandle.SetCustomIcon('images/y_green.png');
            var pixel = map.LatLongToPixel(points[i], map.GetZoomLevel());

            var title = "N: " + i + "<br />";
            title += points[i].toString();
            title += "<br />";
            title += pixel.x + ", " + pixel.y;
            dragHandle.SetTitle(title);
            Geo.layers.SelectionMarkers.AddShape(dragHandle);
        }
    };
    */
    this.DetachSelectors = function () {
        map.DetachEvent("onclick", Geo.Selection.SelectPolygonsForAction);
    };

    this.CleanUpHTML = function () {
        document.getElementById("pol0").innerHTML = "";
        document.getElementById("polArea").innerHTML = "";
        document.getElementById("pol10").innerHTML = "";
        document.getElementById("pol11").innerHTML = "";
    };
    
}();