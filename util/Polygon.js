/*
 * This is a fairly obvious class.
 * Functionality related to managing polygons
 * TODO: There seems to be some unused dependency on "map" param ...
 * TODO: this.handles is not obvious enough, but simply used in some functions
 * 
 * @param {type} points
 * @param {type} map
 * @returns {Polygon}
 */
/* global Vector2D, Line */

function Polygon(points, map) {
    if ( arguments.length === 2 ) {
        if ( arguments[1] === "vemap")
        {
            this.initFromBingMap(points);
        } else {
            throw new Error ("Only constructor for VEMap is implemented at this moment");
        }
        
    } else
    {
        throw new Error ("Must pass 2 parameters. Need to pass first param 'points' array and second param to classify the 'points' ");
    }
}

Polygon.prototype.initFromBingMap = function (points) {
    if(points.length && points.length > 2){
        this.handles = [];
        //Bing polygon has 1st and last points the same (closed polygon)
        // I'd rather have it stripped, thus points.length -1
        for ( var i = 0; i < points.length-1; i++) {
            var x = parseFloat( points[i].Longitude );
            var y = parseFloat( points[i].Latitude );
            this.handles.push( new Point2D(x, y) );
        }
    } else {
        throw new Error ("Invalid polygon initialization");
    }
	
};
//Polygon.prototype.init = function(points) {
//
//	if(points.length && points.length >2){
//        // Init properties
//        var points = svgNode.getAttributeNS(null, "points").split(/[\s,]+/);
//
//        this.handles = new Array();
//        for ( var i = 0; i < points.length; i += 2) {
//            var x = parseFloat( points[i]   );
//            var y = parseFloat( points[i+1] );
//            
//            this.handles.push( new Handle(x, y, this) );
//        }
//    } else {
//        throw new Error("Invalid polygon initialization");
//    }
//};
///////////////////////////////////////////////////////////
/*
 * Is polygon cancave
 * @returns {Boolean}
 */
Polygon.prototype.isConcave = function() {

    var positive = 0;
    var negative = 0;
    var length = this.handles.length;
    for ( var i = 0; i < length; i++) {
        var p0 = this.handles[i];
        var p1 = this.handles[(i+1) % length];
        var p2 = this.handles[(i+2) % length];
        var v0 = Vector2D.fromPoints(p0, p1);
        var v1 = Vector2D.fromPoints(p1, p2);
        var cross = v0.cross(v1);

        if ( cross < 0 ) {
                negative++;
        } else {
                positive++;
        }
    }

    return ( negative !== 0 && positive !== 0 );
    
};
/*
 * Is polygon convex
 * @returns {Boolean}
 */
Polygon.prototype.isConvex = function() {
    return !this.isConcave();
};

/*
 * Static function to get the index of the lowest point in a polygon
 * x is longitude 
 * y is latitude 

 * @params {Array} Array containing point of the polygon
 * 
 * @returns {int} Array index of the lowest point
 */
Polygon.getLowestPointIndex = function (polygon) {
    var length = polygon.length;

    var resI;
    var xMin, yMin;

    for (var i = 0; i < length; i++) {
        if (i === 0) {
            xMin = polygon[i].x;
            yMin = polygon[i].y;
            resI = 0;
            continue;
        }
        if (polygon[i].y < yMin) {
            xMin = polygon[i].x;
            yMin = polygon[i].y;
            resI = i;
        } else if (polygon[i].y === yMin) {
            if (polygon[i].x > xMin) {
                xMin = polygon[i].x;
                yMin = polygon[i].y;
                resI = i;
            }
        }
    }

    return resI;
};

/*
 * Static method to get orientation of a polygon
 * 
 * A slightly faster method is based on the observation that it isn't necessary to compute the area. 
 * Find the lowest vertex (or, if there is more than one vertex with the same lowest coordinate, 
 * the rightmost of those vertices) and then take the cross product of the edges fore and aft of it. 
 * Both methods are O(n) for n vertices, but it does seem a waste to add up the total area when a single cross product
 * (of just the right edges) suffices. Code for this is available at ftp://cs.smith.edu/pub/code/polyorient.C (2K).
 * 
 * The reason that the lowest, rightmost (or any other such extreme) point works is that
 * the internal angle at this vertex is necessarily convex, strictly less than 
 * pi (even if there are several equally-lowest points). 
 */
Polygon.getOrientation = function (polygon) {
   /*
    if (Intersection.getArea(polygon) > 0){
    return "counterclockwise";
    } else {
    return "clockwise";
    }
    */
   var length = polygon.length;

   var lpi = Polygon.getLowestPointIndex(polygon);

   var pb = lpi - 1;
   var pa = lpi + 1;

   if (lpi === 0) {
       pb = length - 1;
   } else if (lpi === length - 1) {
       pa = 0;
   }

   var orient = Vector2D.get3PProduct(polygon[pb], polygon[lpi], polygon[pa]);
   
   //TODO: work out orientation into separate data structure
   if (orient > 0) {
       return "counterclockwise";
   } else {
       return "clockwise";
   }
};

/* 
 * Static method to change orientation of the polygon
 * 
 * @params {Array} Array containing polygon pointes
 * 
 * @returns {Array} The same polygon with direction changed
 */
Polygon.changeOrientation = function (polygon) {
   polygon = polygon.reverse();
   var fp = polygon.pop();
   polygon.unshift(fp);
   return polygon;
};

/* 
 * Static method to determine if point is on boundary of a polygon
 * Discussion on it in SF:
 * http://stackoverflow.com/questions/328107/how-can-you-determine-a-point-is-between-two-other-points-on-a-line-segment#328337
 * 
 * @params {Point2D} point in question
 * @params {Array} Array containing points of a polygon
 * 
 * @returns {Boolean}
 */
Polygon.isPointOnBoundary = function (point, points) {
   var retVal = false;
   var length = points.length;
   for (var i = 0; i < length; i++) {
       var a1 = points[i];
       var a2 = points[(i + 1) % length];
       if(point.isPointOnBoundary(a1, a2)){
           return true;
       }
   }
   return retVal;
};

/*
 * Static method to determine if a given point is within a polygon
 * 
 * @params {Array} Array containing points of a polygon
 * @params {Point2D} point in question
 * 
 * @returns {Boolean}
 */
Polygon.isPointInPolygon = function (points, point) {
   var length = points.length;
   var counter = 0;
   var x_inter;

   var p1 = points[0];
   for (var i = 1; i <= length; i++) {
       var p2 = points[i % length];

       if (point.y > Math.min(p1.y, p2.y)) {
           if (point.y <= Math.max(p1.y, p2.y)) {
               if (point.x <= Math.max(p1.x, p2.x)) {
                   if (p1.y !== p2.y) {
                       x_inter = (point.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y) + p1.x;
                       if (p1.x === p2.x || point.x <= x_inter) {
                           counter++;
                       }
                   }
               }
           }
       }
       p1 = p2;
   }

   return (counter % 2 === 1);
};

/*
 * Static function to get orientation via area (alternative)
 * @param {Array} polygon Array containing polygon points
 * @returns {String}
 */
Polygon.getOrientationArea = function (polygon) {
    var length = polygon.length;
    var area = 0;
    // Do the wrap-around first 
    area = polygon[length - 1].x * polygon[0].y - polygon[0].x * polygon[length - 1].y;

    for (var i = 0; i < length - 1; i++) {
        area += polygon[i].x * polygon[i + 1].y - polygon[i + 1].x * polygon[i].y;
    }

    if (area >= 0.0) {
        return "counterclockwise";
    } else {
        return "clockwise";
    }
};

/*
 * Static function to calculate area of a polygon
 * LIMITATIONS: the function doesn't work for self intersecting polygons
 * REFERENCE: http://www.mathopenref.com/coordpolygonarea2.html
 * 
 * @param {Array} polygon Array containing polygon points
 * @returns {Number}
 */
Polygon.getArea = function (polygon) {
    
     var area   = 0;
     var length = polygon.length;

     for ( var i = 0; i < length; i++ ) {
        var h1 = polygon[i];
        var h2 = polygon[(i+1) % length];

        area += (h1.x * h2.y - h2.x * h1.y);
     }

     return area / 2;
    
};

/*
 * Static function to get polygon-based orientation for a line
 * While somewhat strange (for a line it truly depends on a point of view)
 * the convention allows for things to work
 * 
 * @params {Point2D} point1 The first point defining line
 * @params {Point2D} point2 The second point defining the line
 * 
 * @returns {String}
 */
Polygon.getPolygonOrientationForLine = function (point1, point2) {
   point1.x = parseFloat(point1.x);
   point1.y = parseFloat(point1.y);
   point2.x = parseFloat(point2.x);
   point2.y = parseFloat(point2.y);

   if (point1.y - point2.y > 0) { // line goes down
       return "clockwise";
   } else if (point1.y - point2.y === 0) { // line horizontal
       if (point1.x - point2.x > 0) { // line goes left
           return "clockwise";
       } else {
           return "counterclockwise";
       }
   } else {
       return "counterclockwise";
   }
};

/*
 * Stastic function to determine if two polygons are overlapping
 * 
 * @params {Array} points1 Array representing the first polygon
 * @params {Array} points2 Array representing the second polygon
 * 
 * @returns {Boolean}
 */
Polygon.isOverlapping = function (points1, points2) {
    
    var length = points1.length;

    for (var i = 0; i < length; i++) {
        var a1 = points1[i];
        var a2 = points1[(i + 1) % length]; // this one makes sure that polygon is closed(lastpointIndex+1%length =0
        var inter = Polygon.intersectLinePolygon(a1, a2, points2);

        if (inter.status === "Intersection") {
            return true;
        }
    }
    return false;
};

/*
 * 
 * intersectLinePolygon
 *   
 * @returns {GeoPoints} TODO: It seems like it can return just a 
 *                            regular array of Point2D's
 */
Polygon.intersectLinePolygon = function (a1, a2, points) {
    var pointsCopy = points.slice();
    var result = new GeoPoints();
    var length = points.length;

    var reqDir = Polygon.getPolygonOrientationForLine(a1, a2);
    var orientation = Polygon.getOrientation(pointsCopy);

    if (orientation !== reqDir) {
        points = Polygon.changeOrientation(pointsCopy);
    }

    for (var i = 0; i < length; i++) {
        var b1 = points[i];
        var b2 = points[(i + 1) % length];
        var inter = Line.getIntersectionPoint(a1, a2, b1, b2);
        result.appendPoints(inter.points);
    }

    if (result.points.length > 1) {

        if (a1.y - a2.y > 0) { // line goes down
            result.points.sort(result.sortUpDown);
            //return "clockwise";
            if (result.points[0].y - result.points[1].y < 0) {
                result.points = result.points.reverse();
            }
        } else if (a1.y - a2.y < 0) { // line goes up
            result.points.sort(result.sortDownUp);
            if (result.points[0].y - result.points[1].y > 0) {
                result.points = result.points.reverse();
            }
        } else { //line horizontal
            if (a1.x - a2.x > 0) { //line goes right to left
                result.points.sort(result.sortRightLeft);
                if (result.points[0].x - result.points[1].x < 0) {
                    result.points = result.points.reverse();
                }
            } else { //line goes left to right
                result.points.sort(result.sortLeftRight);
                if (result.points[0].x - result.points[1].x > 0) {
                    result.points = result.points.reverse();
                }
            }
        }
    }

    return result;
};
    