/* global Geo, Intersection, geoModel */


/* 
 * TODO: It calls Geo.Masks.DrawMask(points)? Again global variable.
 */
Geo.PolygonOperations = new function () { // jshint ignore:line
    
    var localDivId = "polR";
    
    this.DoPolygons = function (action) {
        //TODO: Horrific  global dependency on geoModel (rather should inject it)
        var t1 = Geo.Utils.getMePolyFromMap(geoModel.getPolygonForAction(0));
        var t2 = Geo.Utils.getMePolyFromMap(geoModel.getPolygonForAction(1));

        var rst = Intersection.intersectPolygons(t1, t2, action);

        var na = [];
        na.push("Results:<br />");
        var points = [];
        for (var i = 0; i < rst.polygons.length; i++)
        {
            na.push(rst.polygons[i].points.toString());
            na.push("<br /><br />");
            //////////
            points[i] = [];
            for (var j = 0; j < rst.polygons[i].points.length; j++)
            {
                points[i].push(new VELatLong(rst.polygons[i].points[j].x, rst.polygons[i].points[j].y));
            }
        }
        document.getElementById(localDivId).innerHTML = na;
        Geo.Masks.DrawMask(points);
    };

    this.ClearAll = function () {
        document.getElementById(localDivId).innerHTML = "";
    };
}();