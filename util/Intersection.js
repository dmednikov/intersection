/*
 * 
 * Implementatin of an algorithm by Avraham Margalit and Gary D. Knott
 * 
 * It doesn't include the polygons of type "hole".
 */
/* global Point2D, Polygon, Line, GeoPoints, Algorithm */

var Intersection = new function () { // jshint ignore:line
   
   /* 
     *   Sort of the main function of this object.
     *   
     *   @params {Array} points1 - The first polygon
     *   @params {Array} points2 - The second polygon
     *   @params {String} oper   - Operation to perform on polygons
     */
   this.intersectPolygons = function (points1, points2, oper) {
      var leftPoly = [];
      var rightPoly = [];
      //////////////////////////////////////////////////////////
      var orientationA = Polygon.getOrientation(points1);
      var orientationB = Polygon.getOrientation(points2);
      
      //Normalize the orientations of the input polygons.  
      //Find the relative orientation of the two input polygons A and B, and 
      //change the orientation of polygon B if necessary, according to the 
      //operation and the polygon types as summarized in 
      //Algorithm.StaticDataStructures.OutputPolygon (Table 1)
      if (Algorithm.StaticDataStructures.OutputPolygon[oper] === "same") {
         if (orientationA !== orientationB) {
            points2 = Polygon.changeOrientation(points2);
         }
      } else {
         if (orientationA === orientationB) {
            points2 = Polygon.changeOrientation(points2);
         }
      }
      ///////////////////////////////////////////////////////////
      leftPoly = Intersection.polyIncludeIntersections(points1, points2);
      ///////////////////////////////////////////////////////////
      rightPoly = Intersection.polyIncludeIntersections(points2, points1);
      ///////////////////////////////////////////////////////////
      var fragments = Array();

      var type = Algorithm.StaticDataStructures.EdgeFragments[oper].a;
      fragments = Intersection.extractFragments(fragments, leftPoly, points2, type);
      ///////////////////////////////////////////////////////////
      type = Algorithm.StaticDataStructures.EdgeFragments[oper].b;
      fragments = Intersection.extractFragments(fragments, rightPoly, points1, type);
      ///////////////////////////////////////////////////////////
      var result = Intersection.processFragments(fragments);

      return result;
   };
   /*
     *   intersectPolygonPolygon
     */
   /*
    this.intersectPolygonPolygon = function (points1, points2) {
        var result = Intersection.init("No Intersection");
        var length = points1.length;

        for (var i = 0; i < length; i++) {
            var a1 = points1[i];
            var a2 = points1[(i + 1) % length]; // this one makes sure that polygon is closed(lastpointIndex+1%length =0
            var inter = Intersection.intersectLinePolygon(a1, a2, points2);

            result.appendPoints(inter.points);
            if (inter.status === "Intersection") {
                // check to see if the next points2 is within points1
                // if it is then it needs to be added to result
                if (Polygon.isPointInPolygon(points2, a2)) {
                    result.appendPoints(a2);
                }
            }
        }

        if (result.points.length > 0)
            result.status = "Intersection";

        return result;
    };
    */
   /* 
     *   polyIncludeIntersections
     *   needs both polygons to determine intersections
     */
   this.polyIncludeIntersections = function (points1, points2) {
      var poly = [];
      var j = 0;
      var length = points1.length;

      for (var i = 0; i < length; i++) {
         var a1 = points1[i];
         var a2 = points1[(i + 1) % length]; // this one makes sure that polygon is closed(lastpointIndex+1%length =0

         poly[j] = a1;
            
         if (Polygon.isPointInPolygon(points2, a1)) {
            if (Polygon.isPointOnBoundary(a1, points2)) { // && oper=="union"
               poly[j].setType("outside");
            } else {
               poly[j].setType("inside");
            }
         } else {
            poly[j].setType("outside");
         }
         j++;

         var inter = Polygon.intersectLinePolygon(a1, a2, points2);

         for (var k = 0; k < inter.points.length; k++) {
            poly[j] = inter.points[k];
            poly[j].setType("boundary");
            j++;
         }
      }
      return poly;
   };
   /* 
     *   extractFragments
     *   need poly2 for boundary/boundary condition
     */
   this.extractFragments = function (fragments, poly1, poly2, type) {
      var fragment = {};
      var length = poly1.length;
      for (var i = 0; i < length; i++) {
         var a1 = poly1[i];
         var a2 = poly1[(i + 1) % length];
         if (a1.equals(a2)) {
            continue;
         }
         if (a1.type === type || a2.type === type) {
            // insertE(fragment)
            fragment = {
               p0: [a1], 
               p1: [a2]
               };
            fragments.push(fragment);

         } else if (a1.getType() === "boundary" && a2.getType() === "boundary") {
            // get mid point and test if it is in the point2 polygon
            // if it is insertE(fragment)
            var mp = Line.getMidPoint(a1, a2);
            if (Polygon.isPointInPolygon(poly2, mp)) {
               if (type === "inside") {
                  fragment = {
                     p0: [a1], 
                     p1: [a2]
                     };
                  fragments.push(fragment);
               }
            } else {
               if (type === "outside") {
                  fragment = {
                     p0: [a1], 
                     p1: [a2]
                     };
                  fragments.push(fragment);
               }
            }
         }
      }

      return fragments;
   };

   /* 
     *   removeDuplicateFragments
     *   kill duplicate fragments, temp idea for boundary condition cases
     */
   this.removeDuplicateFragments = function (fragments) {

      length = fragments.length;
      var ind = 0;
      while (ind < length) {

         var fragment = fragments[ind];
         var length = fragments.length;
         for (var i = 0; i < length; i++) {
            if (ind !== i && fragment.p0[0].equals(fragments[i].p0[0]) && fragment.p1[0].equals(fragments[i].p1[0])) {
               //delete this fragment
               fragments.splice(i, 1);
               length = fragments.length;
            }
         }
         ind++;
      }
      return fragments;
   };
   /* 
     *   processFragments
     *   
     */
   this.processFragments = function (fragments) {
      fragments = Intersection.removeDuplicateFragments(fragments);

      var result = {};
      result.polygons = [];
      //var length = fragments.length;

      var nfi = 0;
      var polyIndex = 0;
      var newPolyogn = true;
      var foundReversed = false;
      while (fragments.length) {
         var a1 = fragments[nfi];

         fragments.splice(nfi, 1);  // instead of this should just set it as "visited" so that can return multi polygons

         nfi = Intersection.searchNextFragment(a1.p1, fragments);

         // search for points whose orientation could be reversed due to polygon self intersecting
         if (nfi === null) { // if still didn't find anything then it a new polygon
            nfi = Intersection.searchNextFragmentReversed(a1.p1, fragments);
            if (nfi !== null) { // if still didn't find anything then it a new polygon
               var tSwap = fragments[nfi].p0;
               fragments[nfi].p0 = fragments[nfi].p1;
               fragments[nfi].p1 = tSwap;
               foundReversed = true;
            }
         }

         if (newPolyogn === true) {

            result.polygons[polyIndex] = {};
            result.polygons[polyIndex].points = [];

            result.polygons[polyIndex].points.push(new Point2D(a1.p0[0].x, a1.p0[0].y));
            result.polygons[polyIndex].points.push(new Point2D(a1.p1[0].x, a1.p1[0].y));
            newPolyogn = false;

         } else if (nfi === null) {
            nfi = 0;
            newPolyogn = true;
            polyIndex++;

         } else {
            if (fragments.length) {
               result.polygons[polyIndex].points.push(new Point2D(a1.p1[0].x, a1.p1[0].y));

               if (!foundReversed) {
                  foundReversed = false;
               }
            }
         }
      }
      return result;
   };
   /* 
     *   searchNextFragment
     */
   this.searchNextFragment = function (point, fragments) {
      var length = fragments.length;

      for (var i = 0; i < length; i++) {
         var a1 = fragments[i];

         if (a1.p0[0].x === point[0].x && a1.p0[0].y === point[0].y) {
            return i;
         }
      }
      return null;
   };
   /* 
     *   searchNextFragmentReversed
     *   for self intersectecting polygons. where orientation of an original polygon 
     *   could be broken into several orientations of polygons separated at intersection points
     *   and these orientations will be different
     */
   this.searchNextFragmentReversed = function (point, fragments) {
      var length = fragments.length;

      for (var i = 0; i < length; i++) {
         var a1 = fragments[i];

         if (a1.p1[0].x === point[0].x && a1.p1[0].y === point[0].y) {
            return i;
         }
      }
      return null;
   };
        
}();