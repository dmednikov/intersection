/*
 * This is a fairly obvious class
 * Functionality related to managing a point in 2D space
 */

function Point2D(x, y) {
    if ( arguments.length > 0 ) {
        this.x = x;
        this.y = y;
    }
}

Point2D.prototype.toString = function() {
    return this.x +", "+this.y;
};
Point2D.prototype.swap = function() {
    var x = this.x;
    var y = this.y;

    this.x = y;
    this.y = x;
};
Point2D.prototype.equals = function( p2) {
    if (this.x === p2.x && this.y === p2.y){
        return true;
    } else {
        return false;
    }
};
/*
 * There is a concept of a point's type being:
 * - outside
 * - inside
 * - boundary
 * This concept is used for calculations of intersections
 * While it has nothing to do with the Point2D (in purity) the points on which
 * operations are performed are a bit more than just 2D points therfore
 * it doesn't seem unreasonable to store this info here. Perhaps working it
 * into a inherited objects type can be a cleaner solution at some point
 */
Point2D.prototype.setType = function( type ) {
    this.type = type;
};
/*
 * 
 */
Point2D.prototype.getType = function( ) {
    return this.type;
};
/*
 * 
 */
Point2D.prototype.getX = function( ) {
    return this.x;
};
/*
 * 
 */
Point2D.prototype.getY = function( ) {
    return this.y;
};
/*
 * 
 */
Point2D.prototype.collinear = function(a1, a2) {
    var crossproduct = (this.y - a1.y) * (a2.x - a1.x) - (this.x - a1.x) * (a2.y - a1.y);
    return crossproduct;
};
/*
 * 
 */
Point2D.prototype.xCoordIsBetween = function(x1, x2) { 
    // for some reason these are not the same:
    //(0<=0 && 0<=0) evals to true
    //(0<=0<=0) evals to false
    return (x1 <= this.x && this.x <= x2) || (x2 <= this.x && this.x <= x1);
};
/*
 * 
 */
Point2D.prototype.yCoordIsBetween = function(y1, y2) { 
    return (y1 <= this.y && this.y <= y2) || (y2 <= this.y && this.y <= y1);
};
/*
 * 
 */
Point2D.prototype.isPointOnBoundary = function(p1, p2) {
    var crossproduct = this.collinear(p1, p2);
    if (crossproduct === 0 && this.xCoordIsBetween(p1.x, p2.x) && this.yCoordIsBetween(p1.y, p2.y)) {
        return true;
    }
    return false;
};