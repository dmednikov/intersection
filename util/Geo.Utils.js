Geo.Utils = {};

Geo.Utils.getMePoly = function ( )
{
   var result = [];
   //var pl1 = "-118.32734997128485, 34.05709798926945,-118.33665847778322, 34.04298753935195,-118.35042840243692, 34.02210514224573,-118.3397975394839, 34.02210514224573,-118.33889007568361, 34.022786817001986,-118.31691741943361, 34.039289175269076,-118.28962326049805, 34.022502265437836,-118.28897769798763, 34.02210514224573,-118.27343836082638, 34.02210514224573";
   var pla1 = aString.split(",");
   for (var i = 0; i < pla1.length; i = i + 2) {
      result.push(new Point2D(parseFloat(pla1[i + 1]), parseFloat(pla1[i])));
   }
   return result;
};

Geo.Utils.getMePolyFromMap = function ( arr )
{
   var result = [];
   for (var i = 0; i < arr.length; i++) {
      var pla1 = arr[i];
      result.push(new Point2D(parseFloat(pla1.Latitude), parseFloat(pla1.Longitude)));
   }
   //need to make sure that polygon is not closed
   var len = result.length - 1;
   if (result[0].x === result[len].x && result[0].y === result[len].y) {
      result.pop();
   }
   return result;
};

Geo.Utils.SetScaleBarDistanceUnit = function (units)
{
   //VEDistanceUnit.Miles
   map.SetScaleBarDistanceUnit(units);
};

Geo.Utils.GetSelectedLatLongFormat = function ()
{
   var n = $('input[name=latlng]:radio:checked');
   if (n.length == 1)
   {
      return n.val();
   } else {
      return "latlng";
   }
};

Geo.Utils.UserWantsPolygonDrawnAction = function ( )
{
   var ltUSel = Geo.Utils.GetSelectedLatLongFormat();

   var text = $("#polyTA").val();

   var lines = text.match(/^.*((\r\n|\n|\r)|$)/gm);
   //console.info(lines);
   var pNum = lines.length;

   var tempPoints = [];

   for (var i = 0; i < pNum; i++)
   {
      lines[i] = jQuery.trim(lines[i]);
      if (lines[i].length === 0) {

         Geo.buffer.TempPoints = tempPoints;
         //console.info(Geo.buffer.TempPoints);
         Geo.Drawing.DrawPolygon();
         //Geo.buffer.TempPoints = new Array();
         tempPoints = [];
      }
      else if (i == pNum - 1) {
         tempPoints.push(Geo.Utils.ParsePoints(lines[i], ltUSel));
         Geo.buffer.TempPoints = tempPoints;
         //console.info(Geo.buffer.TempPoints);
         Geo.Drawing.DrawPolygon();
         //Geo.buffer.TempPoints = new Array();
         tempPoints = [];
      }
      else {
         tempPoints.push(Geo.Utils.ParsePoints(lines[i], ltUSel));
      }
   }
};
Geo.Utils.ParsePoints = function (points, ltUSel)
{
   var m = points.split(",");
   var lat, lng;
   if (ltUSel == "latlng") {
      lat = parseFloat(jQuery.trim(m[0]));
      lng = parseFloat(jQuery.trim(m[1]));
   } else {
      lng = parseFloat(jQuery.trim(m[0]));
      lat = parseFloat(jQuery.trim(m[1]));
   }
   //tempPoints.push(new VELatLong(45.01188,-111.06687));
   return new VELatLong(lat, lng);
};

Geo.Utils.DrawPolygonFromPoints = function ( )
{
   Geo.Drawing.DrawPolygon();
};

Geo.Utils.ThinPolygon = function ( )
{
   document.getElementById("thinnedPolyPoints").innerHTML = "Not Implemented Yet :-(";
};

Geo.Utils.PopupPolyInfo = function (it)
{
   console.info(it);
};

Geo.Utils.ShapeInfo = function (e)
{
   if (e.elementID !== null)
   {
      var shape = map.GetShapeByID(e.elementID);
      if (shape.Type == "Polygon")
      {
         //shape.SetDescription('<span id="polygonInfo"><img src="images/progress_loading.gif" alt="" align="absmiddle" /> Loading data....</span>');
         var po = new Polygon(shape.GetPoints(), "vemap");
         var tc = po.isConcave() ? "concave" : "convex";
         //Respond to ctrl-click event (toggle polygon icon visibility).
         //Note that pushpin shapes ignore ShowIcon and HideIcon.
         if (e.ctrlKey == 1)
         {
            shape.HideIcon();
         }
         else
         {
            shape.ShowIcon();
         }

         //Display information for the selected shape.
         var info = "";
         info += "polygon: " + tc + "<br />";
         info += "ID (event object): " + e.elementID + "<br />";
         info += "ID (GetID method): " + shape.GetID() + "<br />";
         info += "Type: " + shape.GetType() + "<br />";
         info += "Title: " + shape.GetTitle() + "<br />";
         icon = shape.GetCustomIcon();
         info += "Icon: " + icon + "<br />";

         //Note that the Icon Anchor value for pushpin shapes is always
         //the latitude and longitude of the pin itself.
         info += "Icon Anchor: " + shape.GetIconAnchor() + "<br />";
         $("#polygonInfoBubble").html(info);
      //console.info(document.getElementById(shape.GetID() + '_Content'));
      //document.getElementById(shape.GetID() + '_Content').innerHTML = "asdasd";
      //shape.SetDescription(req.responseText);
      //shape.SetDescription( info );
      }
   }
};
