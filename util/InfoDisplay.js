/*
 * 
 */
/* global geoModel, Geo */

var InfoDisplay = (function () {
    // Constructor
    function InfoDisplay() {
        this.reactToAdditionOfActionPolygon();
        this.reactToClearAll();
    }
    
    InfoDisplay.prototype.reactToAdditionOfActionPolygon = function () {
        $( document ).on( "actionPolygonAdded", {
            foo: "bar"
        }, function( event, polygonId ) {
            //console.log( event.data.foo ); // "bar"
            //console.log( polygonId );           // "bim"
            //console.info(this);
            displayPolygonCoordinates.call(this, polygonId);

        });
    };
    
    function displayPolygonCoordinates(id) {
 
        var len = Math.min(2, geoModel._polygonsForAction.length);
        
        for (var index = 0; index < len; index++) {
            var polygon = geoModel.getPolygonForAction(index);
            if (!polygon)
                break;
            var t1Len = polygon.length;
            var disp = "";
            var ltUSel = Geo.Utils.GetSelectedLatLongFormat();
            for (var i = 0; i < t1Len; i++) {
                if (ltUSel === "lnglat") {
                    disp = disp + polygon[i].Longitude + ", " + polygon[i].Latitude + "<br />";
                } else {
                    disp = disp + polygon[i] + "<br />";
                }

            }
            var m = index + 10;
            document.getElementById("pol" + m).innerHTML = disp;
        }
        
    }
    
    InfoDisplay.prototype.reactToClearAll = function () {
        $( document ).on( "clearAll", function( ) {
            cleanUpHTML.call(this);
        });
    };
    
    function cleanUpHTML () {
        document.getElementById("pol0").innerHTML = "";
        document.getElementById("polArea").innerHTML = "";
        document.getElementById("pol10").innerHTML = "";
        document.getElementById("pol11").innerHTML = "";
    }
    
    return InfoDisplay;
})();

var infoDisplay = new InfoDisplay();