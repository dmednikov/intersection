/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Line(points) {
    if ( arguments.length >= 2 ) {
        throw new Error ("There is an error initializing Line");
    } 
}

/*
 * TODO: It currently returns {points:[]} which is kind of strange
 *       all is needed is a point not an object with a member and array in it
 *       Perhaps re-work it to return point or NaN; Or somethng like that
 *       
 * Calculates and returns intersection point between two lines.
 * 
 * @param {Point2D} a1 - First point on line 1
 * @param {Point2D} a2 - Second point on line 1
 * @param {Point2D} b1 - First point on line 2
 * @param {Point2D} b2 - Second point on line 2
 * 
 * @returns {Object} Returns object with points array as a member. If there is an intersection
 * points array will have intersection point in it. {points:[]}
 */
Line.getIntersectionPoint = function (a1, a2, b1, b2) {
    a1.x = parseFloat(a1.x);
    a1.y = parseFloat(a1.y);

    a2.x = parseFloat(a2.x);
    a2.y = parseFloat(a2.y);

    b1.x = parseFloat(b1.x);
    b1.y = parseFloat(b1.y);

    b2.x = parseFloat(b2.x);
    b2.y = parseFloat(b2.y);
    
    if(isNaN(a1.x) || isNaN(a1.y) || isNaN(a2.x) || isNaN(a2.y) || 
            isNaN(b1.x) || isNaN(b1.y) || isNaN(b2.x) || isNaN(b2.y)){
        throw new Error ("Value passed into getIntersectionPoint was not a valid float.");
    }

    var result ={};
    result.points = [];
    // 
    var ua_t = (b2.x - b1.x) * (a1.y - b1.y) - (b2.y - b1.y) * (a1.x - b1.x);
    var ub_t = (a2.x - a1.x) * (a1.y - b1.y) - (a2.y - a1.y) * (a1.x - b1.x);
    var u_b = (b2.y - b1.y) * (a2.x - a1.x) - (b2.x - b1.x) * (a2.y - a1.y);

    if (u_b !== 0) {
        var ua = ua_t / u_b;
        var ub = ub_t / u_b;

        if (0 <= ua && ua <= 1 && 0 <= ub && ub <= 1) {
            //return intersection point
            result.points.push(
                new Point2D(
                    a1.x + ua * (a2.x - a1.x),
                    a1.y + ua * (a2.y - a1.y)
                    )
                );
        } else {
            //result = Intersection.init("No Intersection");
        }
    } else {
        if (ua_t === 0 || ub_t === 0) {
            //result = Intersection.init("Coincident");
        } else {
            //result = Intersection.init("Parallel");
        }
    }

    return result;
};

/*
* Calculates middle point of a line
* @param {Point2D} point1
* @param {Point2D} point2
* @returns {Point2D}
*/
Line.getMidPoint = function (point1, point2) {

   point1.x = parseFloat(point1.x);
   point1.y = parseFloat(point1.y);

   point2.x = parseFloat(point2.x);
   point2.y = parseFloat(point2.y);
   
   if(isNaN(point1.x) || isNaN(point1.y) || 
            isNaN(point2.x) || isNaN(point2.y)){
        throw new Error ("Value passed into getMidPoint was not a valid float.");
    }
    
   var x = (point1.x + point2.x) / 2;
   var y = (point1.y + point2.y) / 2;
   return new Point2D(x, y);
};