Geo.Masks = {};
/*
 * 
 */
Geo.Masks.DrawMask = function (points) {
    //map.DeleteShapeLayer(MaskLayer);
    try {
        map.AddShapeLayer(Geo.layers.PolygonOperationsMasks);
    }
    catch (e) {
    }
    Geo.layers.PolygonOperationsMasks.DeleteAllShapes();
    var tempo;
    for (var i = 0; i < points.length; i++) {
        tempo = new VEShape(VEShapeType.Polygon, points[i]);
        tempo.HideIcon();
        tempo.SetLineColor(new VEColor(255, 0, 0, 0.5));
        tempo.SetFillColor(new VEColor(0, 0, 128, 0.3));
		tempo.SetZIndex(null, 51);
        tempo.Primitives[0].symbol.stroke_dashstyle = "Dash";
        Geo.layers.PolygonOperationsMasks.AddShape(tempo);
    }
};
/*
 * 
 */
Geo.Masks.ClearAll = function () {
    Geo.layers.PolygonOperationsMasks.DeleteAllShapes();
};