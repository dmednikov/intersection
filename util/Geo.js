/*
 * Global variable Geo for maintaining various structures in memory
 * 
 * TODO: Need better descriptions for every structure in here ...
 */
var Geo = {};
Geo.layers = {};
//Geo.layers.DrawnShapes =  new VEShapeLayer(); 
Geo.layers.SelectionMarkers =  new VEShapeLayer();
Geo.layers.PolygonOperationsMasks =  new VEShapeLayer();

Geo.polygons = [];
Geo.shapes = [];

//Geo.index = 0;// I do not think this one is used anywhere

//Geo.tempPoints = new Array(); // I do not think this one is used
//Geo.polygonsForAction = [];
// these are used for drawing exclusively, so they were moved to Draw
//Geo.buffer = {};
//Geo.buffer.TempShape = {}; // what is this for?
//Geo.buffer.TempPoints = []; // while drawing need to store points somewhere