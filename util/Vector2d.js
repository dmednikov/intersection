/*
 * Functionality for the vector manipulation in 2D space
 * 
 * @param {type} x
 * @param {type} y
 * @returns {Vector2D}
 */
function Vector2D(x, y) {
    if ( arguments.length > 0 ) {
        this.x = x;
        this.y = y;
    }
}
/*
 * Length of the Vector
 * 
 * @returns {Number}
 */
Vector2D.prototype.length = function() {
    return Math.sqrt(this.x*this.x + this.y*this.y);
};
/*
 * Dot product of Vectors
 * 
 * @param {Vector2D} vector
 * @returns {Number}
 */
Vector2D.prototype.dot = function(vector) {
    return this.x*vector.x + this.y*vector.y;
};
/*
 * Cross product of Vectors
 * 
 * @param {Vector2D} vector
 * @returns {Number}
 */
Vector2D.prototype.cross = function(vector) {
    return this.x*vector.y - this.y*vector.x;
};
/*
 * Convert Vector to string
 * 
 * @returns {String}
 */
Vector2D.prototype.toString = function() {
    return this.x + "," + this.y;
};
/*
 * Create new Vector2D from 2 Point2D's
 * 
 * @returns {Vector2D}
 */
Vector2D.fromPoints = function(p1, p2) {
    return new Vector2D(
        p2.x - p1.x,
        p2.y - p1.y
    );
};

/*
 * Static method to calculate a cross product based on 3 points
 * 
 * @param {Point2D} point 1
 * @param {Point2D} point 2
 * @param {Point2D} point 3
 * 
 * @returns {float}
 */
Vector2D.get3PProduct = function (p1, p2, p3) {
    var s1 = p2.x * p3.y + p1.x * p2.y + p1.y * p3.x;
    var s2 = p1.y * p2.x + p2.y * p3.x + p1.x * p3.y;

    return s1 - s2;
};
