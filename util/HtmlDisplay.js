/*
 * Deals with some enabling/disbling of the HTML controls
 * in response to specific events.
 */

var HtmlDisplay = (function () {
   // Constructor
   function HtmlDisplay() {
      this.reactToPolygonsForActionAreReady();
      this.reactToPolygonsForActionAreNotReady();
	  this.reactToActionPolygonsUpdated();
   }
   
   HtmlDisplay.prototype.reactToActionPolygonsUpdated = function () {
      $( document ).on( "actionPolygonsUpdated", 
         function( ) {
            disableSetOperationsButtons.call(this);
         });
   };
   
   HtmlDisplay.prototype.reactToPolygonsForActionAreReady = function () {
      $( document ).on( "actionPolygonsAreReadyForOperations", 
         function( ) {
            enableSetOperationsButtons.call(this);
         });
   };
    
   function enableSetOperationsButtons(id) {
      var inputs = $(".setOperationButtons > input");
      $.each( inputs, function( key, value ) {
         $(value).prop('disabled', false);
      });
   }
    
   HtmlDisplay.prototype.reactToPolygonsForActionAreNotReady = function () {
      $( document ).on( "actionPolygonsAreNotReadyForOperations", function( ) {
         disableSetOperationsButtons.call(this);
      });
   };
    
   function disableSetOperationsButtons(id) {
      var inputs = $(".setOperationButtons > input");
      $.each( inputs, function( key, value ) {
         $(value).prop('disabled', true);
      });
   }
    
   return HtmlDisplay;
})();

var htmlDisplay = new HtmlDisplay();