/*
 * This is the main model class
 * Private/public discussions:
 * http://philipwalton.com/articles/implementing-private-and-protected-members-in-javascript/
 * https://en.wikipedia.org/wiki/Immediately-invoked_function_expression
 * 
 * The call function allows us to call the private function with the appropriate context (this).
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/call
 */

var GeoModel = (function () {
    // Constructor
    function GeoModel() {

        this._layers = {};
        this._layers.DrawnShapes = new VEShapeLayer();
        this._layers.SelectionMarkers = new VEShapeLayer();
        this._layers.PolygonOperationsMasks = new VEShapeLayer();

        this._polygons = [];
        this._shapes = [];

        this._polygonsForAction = [];
        
        this.reactToClearAll();
        
    }

    GeoModel.prototype.getDrawnShapsLayer = function () {
        return this._layers.DrawnShapes;
    };
    /*
     * 
     */
	 /*
    GeoModel.prototype.addPolygonForAction = function (polygon) {
        if (!isPolygonAlreadySelected.call(this, polygon.id) && this._polygonsForAction.length < 2) {
            this._polygonsForAction.push(polygon);
            
            $( document ).trigger( "actionPolygonAdded", [ polygon.id ] );
        }
        
        if( 2 === this._polygonsForAction.length ) {
           $( document ).trigger( "actionPolygonsAreReadyForOperations" );
        }
    };
	*/
    /*
     * 
     */
    GeoModel.prototype.handlePolygonForActionArray = function (polygon) {
        if (isPolygonAlreadySelected.call(this, polygon.id) ) {
            //in this case a polygon needs to be removed from the array
			removePolygonFromPolygonActionArray.call(this, polygon.id);
			
			$( document ).trigger( "actionPolygonsUpdated" );
        } else if (!isPolygonAlreadySelected.call(this, polygon.id) && this._polygonsForAction.length < 2) {
            this._polygonsForAction.push(polygon);
            
            $( document ).trigger( "actionPolygonAdded", [ polygon.id ] );
			$( document ).trigger( "actionPolygonsUpdated" );
        }
		
        if( 2 === this._polygonsForAction.length ) {
           $( document ).trigger( "actionPolygonsAreReadyForOperations" );
        }
    };

    /*
     *
     */
    GeoModel.prototype.getPolygonForAction = function (index) {
        return this._polygonsForAction[index].polygon;
    };

    GeoModel.prototype.reactToClearAll = function () {
        $( document ).on( "clearAll", { cop: "drop", context: this } ,function( event ) {
            resetGeoModel.call(event.data.context, event);
        });
    };
    
    function resetGeoModel (event) {
        this._polygonsForAction = [];
        $( document ).trigger( "actionPolygonsAreNotReadyForOperations" );
    }
    
    function isPolygonAlreadySelected(id) {
        var len = this._polygonsForAction.length;

        for (var i = 0; i < len; i++) {
            if (id === this._polygonsForAction[i].id) {
                return true;
            }
        }
        return false;
    }
	function removePolygonFromPolygonActionArray(id) {
        var len = this._polygonsForAction.length;
		var i = 0;
		
        for (i = 0; i < len; i++) {
            if (id === this._polygonsForAction[i].id) {
                break;
            }
        }
		this._polygonsForAction.splice( i, 1 );
		//delete this._polygonsForAction[i]; delete leaves an undefined element in the array, cannot use it
    }

    return GeoModel;
})();

var geoModel = new GeoModel();