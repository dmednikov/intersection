/*
 * Utility object to help with some of the commmon functionality on Points array
 * For example appending and sorting points
 */
function GeoPoints(status) {
    this.points = [];
}
GeoPoints.prototype.toString = function () {
    return this.points.toString();
};
GeoPoints.prototype.appendPoints = function (points) {
    this.points = this.points.concat(points);
};
GeoPoints.prototype.sortUpDown = function (p1, p2) {
    return p2.y - p1.y;
};
GeoPoints.prototype.sortDownUp = function (p1, p2) {
    return p1.y - p2.y;
};
GeoPoints.prototype.sortLeftRight = function (p1, p2) {
    return p2.x - p1.x;
};
GeoPoints.prototype.sortRightLeft = function (p1, p2) {
    return p1.x - p2.x;
};