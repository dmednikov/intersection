module.exports = function (grunt) {

    grunt.initConfig({
        qunit: {
            all: ['test/**/*.html']
        },
        jshint: {
            files: ['Gruntfile.js', 'util/**/*.js', 'test/**/*.js'],
            options: {
                globals: {
                    jQuery: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-qunit');

    grunt.registerTask('default', ['jshint']);
    // A convenient task alias.
    grunt.registerTask('test', ['qunit']);
};